CREATE DATABASE DBPenerbit318

use DBPenerbit318

create table tblPengarang(
ID int identity(1,1)not null,
Kd_Pengarang varchar(7) primary key not null,
Nama varchar(30) not null,
Alamat varchar(80) not null,
Kota varchar(15) not null,
Kelamin varchar(1) not null
)

--2. bikin vwPengarang 
create view vwPengarang
as
select pengarang.Kd_Pengarang, pengarang.Nama, pengarang.Kota
from tblPengarang as pengarang 

--3. insert data ke dalam tblPengarang
insert into tblPengarang(Kd_Pengarang,Nama,Alamat,Kota,Kelamin)
values
('P0001','Ashadi', 'Jl. Beo 25', 'Yogya', 'P'),
('P0002','Rian', 'Jl. Solo 123', 'Yogya', 'P'),
('P0003','Suwadi', 'Jl. Semangka 13', 'Bandung', 'P'),
('P0004','Siti', 'Jl Durian 15', 'Solo', 'W'),
('P0005','Amir', 'Jl. Gajah 33', 'Kudus', 'P'),
('P0006','Suparman', 'Jl. Harimau 25', 'Jakarta', 'P'),
('P0007','Jaja', 'Jl. Singa 7', 'Bandung', 'P'),
('P0008','Saman', 'Jl. Naga 12', 'Yogya', 'P'),
('P0009','Anwar', 'Jl. Tidar 6A', 'Magelang', 'P'),
('P0010','Fatmawati', 'Jl. Renjana 4', 'Bogor', 'W')

select * from tblPengarang

--a. tampilkan (Kd_Pengarang, nama) yang dikelompokan atas nama 
select Nama, Kd_Pengarang
from tblPengarang 
order by nama ASC

-- b. tampilkan (kota, Kd_Pengarang, nama) yang dikelompokkan atas kota
select Kota, Kd_Pengarang, Nama
from tblPengarang
order by Kota ASC

--c. hitung dan tampilkan jumlah pengarang 
select count(nama) as jumlahPengarang
from tblPengarang

--d. tampilkan record kota dan jumlah kotanya 
select Kota, count(kota) jumlahKota
from tblPengarang
group by Kota

--e. tampilkan kota yang muncul lebih dari 1
select kota, count(kota) jumlahKota
from tblPengarang
group by Kota
having count(kota) > 1

--f. tampilkan kd_pengarang yang terbesar dan terkecil
select min(kd_Pengarang) as terkecil, max(Kd_Pengarang) as terbesar
from tblPengarang 

--4. create table tblGaji
create table tblGaji(
ID int primary key not null,
Kd_Pengarang varchar(7) not null,
Nama varchar(30) not null,
Gaji decimal(18,4) not null
)

--5. insert tblGaji
insert into tblGaji(ID,Kd_Pengarang,Nama,Gaji)
values
(1,'P0002', 'Rian', '600000'),
(2,'P0005', 'Amir', '700000'),
(3,'P0004', 'Siti', '500000'),
(4,'P0003', 'Suwadi', '1000000'),
(5,'P0010', 'Fatmawati', '600000'),
(6,'P0008', 'Saman', '750000')

--5a. tampilkan gaji tertinggi dan terendah 
select max(gaji) as tertinggi, min(gaji) as terendah
from tblGaji

--5b. tampilkan gaji diatas 600000
select gaji
from tblGaji
where gaji > 600000

--5c. tampilkan jumlah gaji
select sum(gaji) as jumlahGaji
from tblGaji

--5d. tampilkan jumlah gaji berdasarkan kota
select Kota, sum(gaji) as jumlahGaji
from tblGaji as gj
join tblPengarang as p on gj.Kd_Pengarang = p.Kd_Pengarang
group by Kota 

--5e. tampilkan seluruh record pengarang anatara p0001-p0006 dari table pengarang
select *
from tblPengarang
where  Kd_Pengarang between 'P0001' and 'P0006'

--5f. tampilkan seluruh data yogya,solo dan magelang dari table pengarang
select *
from tblPengarang
where kota='Yogya'or kota='solo' or kota='magelang'

--5g. tampilkan seluruh data yang bukan yogya dari table pengarang
select *
from tblPengarang
where kota not like 'Yogya'

--5h. tampilkan data pengarang yang namanya :
--a. dimulai dari huruf (a)
select * 
from tblPengarang 
where nama like'a%'

--b. berakhiran(i)
select * 
from tblPengarang 
where nama like'%i'

--c. huruf ketiganya(a)
select * 
from tblPengarang 
where nama like'__a%'

--d. tidak berakhiran(n)
select * 
from tblPengarang 
where nama  not like'%n'

--5i. tampilkan seluruh data tblPengarang dan tblGaji dengan kd_pengarang yang sama
select * 
from tblPengarang as p
join tblGaji as gj on p.Kd_Pengarang = gj.Kd_Pengarang

--5j. tampilkan kota yang memiliki gaji dibawah 1000000
select distinct kota
from tblPengarang as peng
join tblGaji as gj on peng.Kd_Pengarang = gj.Kd_Pengarang
where gaji < 1000000

--5k. ubah panjang dari tipe kelamin menjadi 10
alter table tblPengarang alter column kelamin varchar(10)

--5l. tambahkan kolom[gelar] dengan tipe data varchar(12) pd tblPengarang
alter table tblPengarang add Gelar varchar(12)

--5m. ubah alamat dan kota dari rian di tblPengarang menjadi, jl.Cendrawasih 65 dan pekanbaru
update tblPengarang set alamat='Jl. Cendrawasih 65', Kota='Pekanbaru' where Nama = 'Rian'
select * from tblPengarang where Nama = 'Rian'

-------------------------------------------------------------------------------------------------------------
--TASK 4 DBSELLING--

--1. create database dbselling

create database DBSelling318

use DBSelling318

create table tblItem
(
tblItemID int identity(1,1) not null,
ItemCode varchar(13) primary key not null,
ItemName varchar(30) not null,
BuyingPrice Decimal(18,4) not null,
SellingPrice Decimal (18,4) not null,
ItemAmount int not null,
Pieces Varchar(15) not null
)

insert into tblItem
values
('IC001', 'Laptop', 1200, 1500, 8,'Unit'),
('IC002', 'Headset', 200, 230, 7,'Unit'),
('IC003', 'Mouse', 260, 300, 8,'Unit'),
('IC004', 'PenDisk 32Gb', 150, 200, 5,'PCS'),
('IC005', 'Cable HDMI', 100, 120, 7,'PCS'),
('IC006', 'Headset Extra Bass', 350, 400, 9,'Unit')

CREATE TABLE tblOfficer
(
tblOfficerID int identity(1,1) not null,
OfficerCode varchar(5) primary key not null,
OfficerName varchar(30) not null,
OfficerPassword varchar(10) not null,
OfficerStatus varchar(15) not null
)

insert into tblOfficer
values
('OC001', 'Fullan Banyu', 'Fullan123', 'Active'),
('OC002', 'Heru Farouksyah', 'Heru123', 'Active'),
('OC003', 'Ingrid Wijaya', 'Inggrid123', 'Active'),
('OC004', 'Budi Cahaya', 'Budi123', 'Active'),
('OC005', 'Alex Timor', 'Alex123', 'Non Active'),
('OC006', 'Anton Kurniawan', 'Anton123', 'Active')

create table tblSelling
(
tblSellingID int identity(1,1) not null,
Invoice varchar(5) primary key not null,
InvoiceDate datetime not null,
Item int not null,
Total decimal(18,4) not null,
Paid Decimal(18,4) not null,







