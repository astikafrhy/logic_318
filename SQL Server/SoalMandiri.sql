--soal mandiri Group 4 ---

--1. Tampilkan produk harga termurah setiap provinsi
select pmhPro.namaProvinsi, pmhPro.namaProduk, pmhPro.Harga
from ProvRatHarga pmhPro
join 
(select kodeProvinsi, namaProvinsi, avg(Harga)Harga
from ProvMaxHarga
group by kodeProvinsi, namaProvinsi) pmh
on pmhPro.kodeProvinsi = pmh.kodeProvinsi and pmhPro.Harga < pmh.Harga
order by pmhPro.KodeProvinsi

CREATE VIEW ProvRatHarga
AS
select pr.Kode as 'KodeProvinsi', pr.Nama as 'namaProvinsi', p.Kode as 'kodeProduk', p.Nama as 'namaProduk', AVG(p.Harga) Harga
from Provinsi pr
join kota k on pr.Kode = k.KodeProvinsi
join Outlet o on o.KodeKota = k.Kode
join Selling s on s.KodeOutlet = o.Kode
join Product p on s.KodeProduct = p.Kode
group by pr.Kode, pr.Nama, p.Kode, p.Nama
--2. tampilkan outlet yg menjual (roti, pasta gigi dan seblak) dengan nama produk,quantity, harga & jumlah penjualan
select ou.Nama as namaOutlet,pr.Nama as namaProduk, sum(sel.Quantity) Quantity, pr.Harga, sum(Quantity * Harga) as [penjualan]
from Selling as sel
join Outlet as ou on sel.KodeOutlet = ou.Kode
join Product as pr on sel.KodeProduct = pr.Kode
join kota as kt on kt.Kode = ou.KodeKota
join provinsi as pro on pro.Kode = kt.KodeProvinsi 
where pr.Kode in('P0192', 'P9289', 'P9830')
group by ou.Nama, pr.Harga, pr.Nama

SELECT * FROM PRODUCT
--3. Buat Nama untuk outlet Daerah Jakarta yang belum memiliki outlet
SELECT
	CONCAT(SUBSTRING(NamaKota, 1,3), SUBSTRING(NamaKota, CHARINDEX(' ',NamaKota,1) + 1,3), ' Jaya') NamaOutlet
FROM (
	SELECT Kota.Nama NamaKota
		FROM Outlet
			RIGHT JOIN Kota ON Outlet.KodeKota = Kota.Kode
		WHERE Outlet.Kode is null
			AND Kota.Nama like '%Jakarta%' 
) KotaNoOutlet

-------------------------------------------------------------------------------------------------------------------------------------------------------------
-- SOAL GROUP 3 --
--1. Tampilkan Kode Produk, Nama Produk dan  jumlah produk diatas rata-rata yang nama produknya berakhiran i dan 
--    mengubah Nama Produk menjadi huruf kapital--

SELECT
	p.Kode, UPPER(p.Nama) NamaProduk,
	SUM(Quantity) JumlahTerjual
FROM
	dbo.Selling s
	JOIN dbo.Product p
		ON p.Kode = s.KodeProduct
GROUP BY
	p.Kode,
	p.Nama
HAVING
	SUM(Quantity) > (SELECT AVG(y.SumQuantity) FROM (SELECT KodeProduct, SUM(Quantity) 
	SumQuantity FROM dbo.Selling GROUP BY KodeProduct) y) AND P.Nama LIKE '%i'

--2. Tampilkan NamaProduk, Harga, dan JumlahTerjual, untuk produk yang belum pernah laku terjual

SELECT 
	p.Nama, 
	p.Harga, 
	ISNULL(SUM(sel.Quantity),0) JumlahTerjual
FROM Product p 
	LEFT JOIN Selling sel ON p.Kode = sel.KodeProduct
WHERE sel.Id  IS NULL
GROUP BY p.nama, p.harga
 
--3. Tampilkan NamaKota dan JumlahTransaksi, untuk kota yang belum pernah terjadi transaksi
SELECT 
	kt.Kode,
	kt.Nama NamaKota, 
	ISNULL(SUM(sel.Quantity),0) JumlahTransaksi
FROM Selling  sel
	RIGHT JOIN Outlet ou ON sel.KodeOutlet = ou.Kode
	RIGHT JOIN kota as kt ON kt.Kode = ou.KodeKota
	JOIN provinsi as pro ON pro.Kode = kt.KodeProvinsi 
--WHERE sel.Id IS NULL
GROUP BY kt.Kode, kt.Nama
HAVING SUM(sel.Quantity) IS NULL

SELECT 
	kt.Kode,
	kt.Nama NamaKota, 
	COUNT(sel.Referensi) JumlahTransaksi
FROM Selling  sel
	RIGHT JOIN Outlet ou ON sel.KodeOutlet = ou.Kode
	RIGHT JOIN kota as kt ON kt.Kode = ou.KodeKota
	JOIN provinsi as pro ON pro.Kode = kt.KodeProvinsi 
--WHERE sel.Id IS NULL
GROUP BY kt.Kode, kt.Nama
HAVING COUNT(sel.Referensi) > 0

-------------------------------------------------------------------------------------------------------
-- GROUP 1 --
--1. Tampilkan hasil penjualan Outlet Jaktim Jaya berdasarkan hari di tahun 2022
select day(s.SellingDate) as 'hari', o.Nama as 'nama_outlet', cast(sum(s.Quantity * p.Harga) as money)
from selling s
join outlet o on s.KodeOutlet = o.Kode
join Product p on s.KodeProduct = p.Kode
where o.KodeKota = 'K1003' and year(s.SellingDate) = 2022
group by day( s.SellingDate), o.Nama

--2. Tampilkan nama produk & jumlah penjualan produk dari outlet Jakpus Jaya
select p.Nama 'namaProduk', cast(sum(s.Quantity * p.Harga )as money) as 'jumlah_penjualan', o.Nama as 'NamaOutlet' 
from selling s
join outlet o on s.KodeOutlet = o.Kode
join Product p on s.KodeProduct = p.Kode
where o.KodeKota = 'K1001'
group by p.Nama, o.Nama

--3. Tampilkan nama produk yang terjual di bulan Januari
select year(s.SellingDate) Tahun, DATENAME(month, s.SellingDate) Bulan, p.Nama as 'NamaProduk' 
from Selling s
join Product p on s.KodeProduct = p.Kode
where month(s.SellingDate) = 1
group by year(s.SellingDate), DATENAME(month, s.SellingDate), p.Nama

------------------------------------------------------------------------------------------------------------------------------------------------------
--SOAL GROUP 2 --
-- 1.  Tampilkan nama barang, harga barang dan tambahkan kolom dengan keterangan jika diatas rata-rata maka upper, jika dibawah rata2 maka lower

	SELECT
		Product.Nama NamaProduk,
		Product.Harga HargaProduk,
		CASE
			WHEN Product.Harga > 
				(SELECT
					AVG (Product.Harga)
				FROM
					Product) THEN 'Upper'
			WHEN Product.Harga < (
				SELECT
					AVG (Product.Harga)
				FROM
					Product) THEN 'Lower'
		END Keterangan
	FROM Product


-- 2.  Tampilkan Produk yang terjual di bulan dan hari yang sama
	SELECT
		Product.Nama Namaproduk
	FROM Selling
	JOIN Product ON Selling.KodeProduct = Product.Kode
	JOIN 
	(
		SELECT
			DAY (Selling.SellingDate) Hari,
			MONTH (Selling.SellingDate) Bulan
		FROM Selling
		GROUP BY Selling.SellingDate
		HAVING COUNT(*) > 1
	) SellDateDupe ON SellDateDupe.Hari = DAY(Selling.SellingDate) AND SellDateDupe.Bulan = MONTH(Selling.SellingDate)
	GROUP BY Product.Kode, Product.Nama

	--cara2
SELECT Distinct *
FROM vwDayMonthProduk dayPro
JOIN
	(
		SELECT *
		FROM vwDayMonthProduk
	) sub
ON dayPro.Hari = sub.Hari AND dayPro.Bulan = sub.Bulan AND dayPro.KodeProduct <> sub.KodeProduct

CREATE VIEW vwDayMonthProduk
AS
SELECT KodeProduct, DAY(SellingDate) Hari, MONTH(SellingDate) Bulan 
FROM 
Selling

-- 3.  Tampilkan Frekuensi terjualnya suatu produk dengan menampilkan tanggal dan quantitynya

	SELECT
		Product.Nama NamaProduk,
		Selling.SellingDate Tanggal,
		COUNT(Product.Kode) Frekuensi,
		SUM(Selling.Quantity) JumlahBarangTerjual
	FROM Selling
	JOIN Product ON Selling.KodeProduct = Product.Kode
	GROUP BY Product.Kode, Product.Nama, Selling.SellingDate