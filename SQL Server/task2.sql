-- task 2 selling product --
--Soal SQL
--Note jumlah penjualan = qty * hrg, jumlah produk = qty

--01 Tapilkan jumlah penjualan barang peroutlet per-tanggal

use InventoryDb318

select p.Nama as nama_barang, o.Nama as Nama_Outlet, s.SellingDate, sum(s.Quantity * p.Harga) as jumlah_penjualan 
from Selling as s
join Outlet as o on o.Kode = s.KodeOutlet
join Product as p on p.Kode = s.KodeProduct
group by p.Nama, o.Kode, s.SellingDate, o.Nama
order by s.SellingDate

--02 Tapilkan jumlah penjualan per tahun

select * from Selling
select 
   sum(Quantity) as 'Jumlah Penjualan',  
   year(SellingDate) as 'Tahun'
from Selling
group by year(SellingDate)

--03 Tapilkan jumlah product terlaris dan ter tidak laris per kota
select t1.NamaProduct, t1.NamaKota ,t1.KodeKota,  max(t1.Quantity) as 'Terlaris', min(t1.Quantity) as 'Tidak Terlaris'
from
	(select sel.KodeProduct, sum(sel.Quantity) as 'Quantity', kt.kode as 'KodeKota', kt.Nama as 'NamaKota', pr.Nama as 'NamaProduct'
	from selling as sel
		join Outlet as ou on ou.Kode = sel.KodeOutlet
		join kota as kt on kt.Kode = ou.KodeKota
		join Product as pr on pr.Kode = sel.KodeProduct
	group by sel.KodeProduct, kt.Kode, kt.Nama, pr.Nama) t1
	group by t1.KodeKota, t1.NamaKota, t1.NamaProduct

--04 Tapilkan jumlah penjualan per provinsi dan urutkan dari yang terbesar

select p.Nama as Provinsi, sum(s.Quantity * pr.Harga) as jumlah_penjualan 
from Selling as s
join product as pr on s.KodeProduct = pr.Kode
JOIN Outlet  outl on outl.Kode = s.KodeOutlet 
JOIN Kota kot on kot.Kode = outl.KodeKota
join Provinsi as p on p.Kode = kot.KodeProvinsi
group by p.Nama
order by sum(Quantity * harga) DESC

--05 Tampilan referensi yang tidak sesuai dengan sellingdate

SELECT Reference, SellingDate
FROM 
(SELECT Referensi Reference,
	SellingDate,
	YEAR(SellingDate) SelYear, 
	MONTH(SellingDate) SelMonth, 
	CAST(SUBSTRING(Referensi, 6,2)as int) dtRef, 
	CAST(SUBSTRING(Referensi, 4,2)as int) yrRef FROM Selling) sel
WHERE  sel.SelMonth <> sel.dtRef AND SelYear <> yrRef

SELECT Referensi,YEAR(SellingDate) Tahun,MONTH(SellingDate) Bulan
FROM Selling sell
WHERE 
	CAST(SUBSTRING(Referensi, 4,2)as int) <> YEAR(SellingDate) AND 
	CAST(SUBSTRING(Referensi, 6,2)as int) <> MONTH(SellingDate)

--06 Tampilan jumlah produk terjual pertahun peroutlet

select 
	ou.Nama,
    sum(Quantity) as 'Jumlah Penjualan',  
    year(SellingDate) as 'Tahun'
from Selling as sel
join Outlet as ou on sel.KodeOutlet = ou.Kode
group by year(SellingDate), ou.Nama

--07 Tampilan jumlah penjualan peroutlet

select 
	ou.Nama,
    sum(Quantity*harga) as 'Jumlah Penjualan'
from Selling as sel
join Outlet as ou on sel.KodeOutlet = ou.Kode
join Product as pr on sel.KodeProduct = pr.Kode
group by  ou.Nama


--08 Tampilan jumlah penjualan per bulan diurutkan berdasar bulan

select 
    sum(Quantity* harga) as 'Jumlah Penjualan',  
    month(SellingDate) as 'Bulan'
from Selling as sel
join Outlet as ou on sel.KodeOutlet = ou.Kode
join Product as pr on sel.KodeProduct = pr.Kode
group by month(SellingDate)

--09 Tampilan rata-rata jumlah penjualan setiap bulan

select 
	month(SellingDate) as 'Bulan',
    avg(Quantity* harga) as 'RataRata'
	--year(sellingDate) as 'Tahun'
from Selling as sel
join Outlet as ou on sel.KodeOutlet = ou.Kode
join Product as pr on sel.KodeProduct = pr.Kode
group by month(SellingDate)--, year(sellingDate)

--10 Tampilan produk dengan jumlah produk dibawah rata-rata
select 
	pr.Kode,
	pr.Nama,
    sum(Quantity) as 'DibawahRataRata'
from Selling as sel
join Product as pr on sel.KodeProduct = pr.Kode
where Quantity < (select AVG(Quantity) from Selling)
group by pr.Kode, pr.Nama

SELECT
	p.Nama NamaProduk,
	sum(Quantity) JumlahTerjual
FROM
	dbo.Selling s
	JOIN dbo.Product p
		ON p.Kode = s.KodeProduct
GROUP BY
	p.Kode,
	p.Nama
HAVING
	sum(Quantity) < (SELECT avg(y.SumQuantity) FROM (SELECT KodeProduct, sum(Quantity) 
	SumQuantity FROM dbo.Selling GROUP BY KodeProduct) y)

--11. Tampilan jumlah outlet per provinsi

select pr.Nama, count(ou.Nama) as [jumlah]
from outlet as ou
join kota as kt on ou.KodeKota = kt.Kode
join Provinsi as pr on kt.KodeProvinsi = pr.Kode
group by pr.Nama

--12.  Tampilan produk terlaris per periode bulanan 
select 
--pr.nama,
	month(SellingDate) as 'Bulan',
    sum(Quantity) as 'Terlaris',
	year(sellingDate) as 'Tahun'
from Selling as sel
join Outlet as ou on sel.KodeOutlet = ou.Kode
join Product as pr on sel.KodeProduct = pr.Kode
where Quantity > (select avg(Quantity) from selling) 
group by month(SellingDate), year(sellingDate)

select selQty.*
from selQty
join (select [Year], [Month], max(Quantity) Quantity
		from SelQty
		group by [Year], [Month]) SelMaxQty
		on SelQty.Quantity = SelMaxQty.Quantity and SelQty.Year = SelMaxQty.year and SelQty.month = SelMaxQty.month 
order by SelQty.year, SelQty.month

create view selQty
as
select year(SellingDate) [Year], MONTH(SellingDate) [Month], KodeProduct, sum(Quantity) Quantity
from Selling
group by year(SellingDate), MONTH(SellingDate), KodeProduct

--13. Tampilkan provinsi yg menjual sampoo, quantity, harga & jumlah penjualan

select pro.Nama, sum(sel.Quantity), pr.Harga, sum(Quantity * Harga) as [penjualan]
from Selling as sel
join Outlet as ou on sel.KodeOutlet = ou.Kode
join Product as pr on sel.KodeProduct = pr.Kode
join kota as kt on kt.Kode = ou.KodeKota
join provinsi as pro on pro.Kode = kt.KodeProvinsi 
where pr.Nama = 'Sampoo'
group by pro.Nama, pr.Harga

--14. Tampilkan produk harga termahal setiap provinsi
select pmhPro.namaProvinsi, pmhPro.namaProduk, pmhPro.Harga
from ProvMaxHarga pmhPro
join 
(select kodeProvinsi, namaProvinsi, max(Harga)Harga 
from ProvMaxHarga
group by kodeProvinsi, namaProvinsi) pmh
on pmhPro.kodeProvinsi = pmh.kodeProvinsi and pmhPro.Harga = pmh.Harga

create view ProvMaxHarga
as
select pr.Kode as 'KodeProvinsi', pr.Nama as 'namaProvinsi', p.Kode as 'kodeProduk', p.Nama as 'namaProduk', MAX(p.Harga) Harga
from Provinsi pr
join kota k on pr.Kode = k.KodeProvinsi
join Outlet o on o.KodeKota = k.Kode
join Selling s on s.KodeOutlet = o.Kode
join Product p on s.KodeProduct = p.Kode
group by pr.Kode, pr.Nama, p.Kode, p.Nama

--15. Tampilkan outlet dengan penjualan tertinggi & terendah

SELECT
		CASE
			WHEN sm.Penjualan = (SELECT * FROM max15) THEN 'Maksimal'
			WHEN sm.Penjualan = (SELECT * FROM min15) THEN 'Minimal'
		END Stat,
		sm.NamaO NamaOutlet,
		sm.Penjualan
	FROM (
		SELECT
			o.Kode KodeO,
			o.Nama NamaO,
			--prod.Kode KodeP,
			ISNULL(SUM(s.Quantity * prod.Harga),0) Penjualan
		FROM dbo.Selling s
		RIGHT JOIN dbo.Outlet o
			ON o.Kode = s.KodeOutlet
		LEFT JOIN dbo.Product prod
			ON prod.Kode = s.KodeProduct
		GROUP BY
			o.Kode,
			o.Nama
	) sm
	WHERE
		sm.Penjualan = (SELECT * FROM max15) OR
		sm.Penjualan = (SELECT * FROM min15)
		--sm.Penjualan = @max OR
		--sm.Penjualan = @min

CREATE VIEW max15
AS
	SELECT
		MAX(mx.Penjualan) Penjualan
	FROM (
		SELECT
			o.Kode KodeO,
			--prod.Kode KodeP,
			SUM(s.Quantity * prod.Harga) Penjualan
		FROM dbo.Selling s
		JOIN dbo.Outlet o
			ON o.Kode = s.KodeOutlet
		JOIN dbo.Product prod
			ON prod.Kode = s.KodeProduct
		GROUP BY
			o.Kode
	) mx

CREATE VIEW min15
AS
	SELECT
		MIN(mn.Penjualan) Penjualan
	FROM (
		SELECT
			o.Kode KodeO,
			--prod.Kode KodeP,
			ISNULL(SUM(s.Quantity * prod.Harga), 0) Penjualan
		FROM dbo.Outlet o
		LEFT JOIN dbo.Selling s
			ON o.Kode = s.KodeOutlet
		LEFT JOIN dbo.Product prod
			ON prod.Kode = s.KodeProduct
		GROUP BY
			o.Kode
	) mn

SELECT MIN(Penjualan) Penjualan FROM
(SELECT
	o.Kode KodeO,
	--prod.Kode KodeP,
	ISNULL(SUM(s.Quantity * prod.Harga), 0) Penjualan
FROM dbo.Outlet o
LEFT JOIN dbo.Selling s
	ON o.Kode = s.KodeOutlet
LEFT JOIN dbo.Product prod
	ON prod.Kode = s.KodeProduct
GROUP BY
	o.Kode) MinO
--GROUP BY KodeO

--16. Tampilkan produk harga termurah setiap provinsi
select pmhPro.namaProvinsi, pmhPro.namaProduk, pmhPro.Harga
from ProvMaxHarga pmhPro
join 
(select kodeProvinsi, namaProvinsi, min(Harga)Harga 
from ProvMaxHarga
group by kodeProvinsi, namaProvinsi) pmh
on pmhPro.kodeProvinsi = pmh.kodeProvinsi and pmhPro.Harga = pmh.Harga
--17. tampilkan outlet yg menjual (roti, pasta gigi dan seblak) dengan nama produk,quantity, harga & jumlah penjualan
select ou.Nama as namaOutlet,pr.Nama as namaProduk, sum(sel.Quantity) Quantity, pr.Harga, sum(Quantity * Harga) as [penjualan]
from Selling as sel
join Outlet as ou on sel.KodeOutlet = ou.Kode
join Product as pr on sel.KodeProduct = pr.Kode
join kota as kt on kt.Kode = ou.KodeKota
join provinsi as pro on pro.Kode = kt.KodeProvinsi 
where pr.Nama in('Roti', 'Pasta Gigi', 'Seblak')
group by ou.Nama, pr.Harga, pr.Nama
--18. Buat Nama untuk outlet Daerah Jakarta yang belum memiliki outlet
SELECT
	CONCAT(SUBSTRING(NamaKota, 1,3), SUBSTRING(NamaKota, CHARINDEX(' ',NamaKota,1) + 1,3), ' Jaya') NamaOutlet
FROM (
	SELECT
	Kota.Nama NamaKota
	FROM Outlet
	RIGHT JOIN Kota ON Outlet.KodeKota = Kota.Kode
	WHERE Outlet.Kode is null
	and Kota.Nama like '%Jakarta%'
) KotaNoOutlet

------------------------------------------------------------------------------------------------------------------------