create database DBPijat

use DBPijat

create table Customer(
ID int identity(1,1) not null,
KodeCustomer varchar(5) primary key not null,
Nama varchar(50) not null,
Alamat varchar(50) not null,
Kota varchar(50) not null,
Provinsi varchar(20) not null,
MemberID varchar(5) not null,
KodeGrade varchar(5) not null
)

insert into Customer
values
('C0001', 'Bayu', 'Jl.Garuda', 'Tangerang Selatan', 'Banten', 'M0982','G0002'),
('C0002', 'Bodi', 'Jl.Merak', 'Bogor', 'Jawa Barat', 'M0981','G0001'),
('C0003', 'Mayang', 'Jl.Titimplik', 'Depok', 'Jawa Barat', 'M0983','G0002'),
('C0004', 'Nina', 'Jl. Galaxy', 'Bekasi', 'Jawa Barat', 'M0984','G0004'),
('C0005', 'Bila', 'Jl.Tanah Abang', 'Jakarta Pusat', 'Jakarta', 'M0985','G0004'),
('C0006', 'Dimas', 'Jl.Gandaria', 'Jakarta Selatan', 'Jakarta', 'M0986','G0003')

select * from Customer

create table Booking(
	ID int identity(1,1),
	Kode varchar(5) primary key,
	MemberId varchar(5),
	Tanggal DateTime
)


insert into Booking values
('B0099','M0982', '04-11-2023'),
('B0097','M0983', '04-18-2023'),
('B0098','M0984', '04-18-2023')

select * from Booking

create table Layanan(
	ID int identity(1,1),
	Kode varchar(5) primary key,
	Tipe varchar(20),
	Keterangan varchar(50),
	Harga decimal(18,2)
)

insert into Layanan
values
('L0101', 'Massage', 'FullBody', 120000),
('L0102', 'Massage', 'HalfBody', 100000),
('L0103', 'Massage', 'Foot Massage',60000),
('L0104', 'Massage', 'Beck Neck Shoulder', 90000),
('L0201', 'Goods', 'Minyak Terapi', 25000),
('L0202', 'Goods', 'Wedang Jahe', 16000),
('L0203', 'Goods', 'Handuk', 46500)

create table Grade(
	ID int identity(1,1),
	Kode varchar(5) primary key,
	Grade varchar(20)
)

select *from grade
insert into Grade
values
('G0001', 'Platinum'),
('G0002', 'Gold'),
('G0003', 'Silver'),
('G0004', 'Bronze')

create table Karyawan(
	ID int identity(1,1),
	Kode varchar(5) primary key,
	Nama varchar(30),
	Role varchar(20)
)

insert into Karyawan 
values
('K0001', 'Wahyu', 'Masseur'),
('K0002', 'Nino', 'Kasir')

create table Transaksi(
	ID int identity(1,1),
	Reff varchar(10) primary key,
	Tanggal datetime,
	KodeKaryawan varchar(5),
	KodeCustomer varchar(5)
)


insert into Transaksi
values 
('M23010923', GETDATE(), 'K0002','C0001'),
('M23010924',getdate(),'K0002','C0002')

create table DetailTransaksi(
	ID int identity(1,1),
	Kode varchar(5) primary key,
	Reff varchar(10),
	KodeKaryawan varchar(5),
	KodeLayanan varchar(5),
	KodeBooking varchar(5),
	Quantity int
)

insert into DetailTransaksi 
values 
('D0001','M23010923','K0001','L0101','B0098',1),
('D0002','M23010923','K0001','L0201','B0098',1),
('D0003','M23010923','K0001','L0202','B0098',1),
('D0004','M23010923','K0001','L0203','B0098',1)



select * from DetailTransaksi

SELECT 
	tr.ID, 
	tr.Tanggal as [Date], 
	tr.Reff, 
	dt.KodeBooking, 
	bk.MemberId, 
	cs.Nama, 
	cs.Alamat,
	cs.Kota,
	cs.Provinsi,
	gr.Grade,
	ly.Tipe,
	ly.Keterangan,
	kr.Nama as Masseur,
	ksr.Nama as Kasir,
	case 
		when ly.Tipe = 'Massage' then (((dt.Quantity * ly.Harga) * 5) / 100)
		else isnull((ly.Harga*0),0)
	end as [Service],

	ly.Harga,
	case 
		when ly.Tipe = 'Massage' then ((ly.Harga * 5) / 100)
		else isnull((ly.Harga*0),0)
	end as [Service],
	((ly.Harga * 11)/100) as tax,
	case 
		when ly.Tipe = 'Massage' then (ly.Harga + ((ly.Harga * 5) / 100) + ((ly.Harga * 11)/100))
		else isnull((ly.Harga+((ly.Harga * 11)/100)),0)
	end as Payment
FROM Transaksi tr
join DetailTransaksi dt on tr.Reff = dt.Reff
join Booking bk on dt.KodeBooking = bk.Kode
join Customer cs on tr.KodeCustomer = cs.KodeCustomer
join Grade gr on cs.KodeGrade = gr.Kode
join Layanan ly on dt.KodeLayanan = ly.Kode
join 
(select k.Kode, k.Nama
		from Karyawan k
		where k.Role = 'Kasir' ) ksr
		 on tr.KodeKaryawan = ksr.Kode
join Karyawan kr on kr.Kode = dt.KodeKaryawan

---------------------------------------------------------------------------------------------------------
--Soal Pijat-pijat
--00 Tampilkan rekapitulasi

select * 
FROM Transaksi tr
join DetailTransaksi dt on tr.Reff = dt.Reff
join Booking bk on dt.KodeBooking = bk.Kode
join Customer cs on tr.KodeCustomer = cs.KodeCustomer
join Grade gr on cs.KodeGrade = gr.Kode
join Layanan ly on dt.KodeLayanan = ly.Kode
join Karyawan kr on kr.Kode = dt.KodeKaryawan

--01 Tampilkan Masseus dengan pelanggan terbanyak

select tr.KodeKaryawan, count(tr.KodeKaryawan) as JumlahPelanggan
from Transaksi tr
join DetailTransaksi dt on tr.Reff = dt.Reff
join Karyawan kr on tr.KodeKaryawan = kr.Kode
join Customer cs on tr.KodeCustomer = cs.KodeCustomer
where kr.Role ='Masseus'
group by tr.KodeKaryawan

select * from Transaksi
select * from Layanan

--02 Tampilkan Produk goods yang paling laris
--03 Tampilkan service massage yang paling sedikit peminat
--04 Tampilkan jumlah service setiap bulannya.
--05 Tampilkan tahun bulan penjualan terbanyak
--06 Tampilkan masing-masing total pendapatan service/massage & product/goods.

select 
	ly.Tipe,
	case 
		when ly.Tipe in ('Massage','Goods') then (ly.Harga + ((ly.Harga * 5) / 100) + ((ly.Harga * 11)/100))
		else isnull((ly.Harga+((ly.Harga * 11)/100)),0)
	end as totalPendapatan
from DetailTransaksi dt
join Layanan ly on  dt.KodeLayanan = ly.Kode

--07 Dalam meningkatkan minat pelanggan, diadakan promo untuk para member dan mendapat potongan.
--		Bronze 2.5% untuk service tertentu
--		Silver 5% untuk service tertentu
--		Gold 5% untuk service & product tertentu
--		Platinum 8% untuk service & product tertentu
--	a. Tambahkan tabel dan/atau kolom diskon per grade dan product.
--	b. Tampilkan transaksi dengan diskon untuk customer sesuai grade-nya.
--08 Tambahkan jenis kelamin untuk member/pelanggan & untuk Masseus, Jenis kelamin char(1) L/P
alter table customer add  jenisKelamin char(1)
update Customer set jenisKelamin = 'L' where KodeCustomer in ('C0001', 'C0002', 'C0006')
update Customer set jenisKelamin = 'P' where KodeCustomer in ('C0003', 'C0004', 'C0005')

alter table karyawan add jenisKelamin char(1)
update Karyawan set jeniskelamin = 'L' where Kode in ('K0001', 'K0002')
--09 Tampilkan jumlah customer per Masseus dan bonus dimana Jumlah customer 5-10 mendapat 5% dari harga service, sedangkan >10 mendapat 8% dari harga serice.
--10 Tampilkan tren jumlah customer harian dengan nama hari (Sunday-Saturday) segala waktu.