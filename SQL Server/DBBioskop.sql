create database DBBioskop318

use DBBioskop318

create table tblFilm(
ID int identity(1,1) not null,
KodeFilm varchar(5) primary key not null,
JudulFilm varchar(100) not null,
Genre varchar(20) not null,
Rating decimal(18,2) not null,
Produksi varchar(55) not null,
Durasi int not null,
Negara varchar(55) not null
)

insert into tblFilm
values
('F0001','IRON MAN','Action', 4.1, 'Marvel', 141, 'Amerika'),
('F0002','IRON MAN 2','Action',3.5,'Marvel',100,'Amerika'),
('F0003','TRAIN TO BUSAN','Horror', 3.2, 'Next Entertainment World', 118, 'Korea'),
('F0004','AVENGER: CIVIL WAR','Action',4.5,'Mervel',184,'Amerika'),
('F0005','THE RAID','Action',3.7,'PT. Merantau Films',154,'Indonesia'),
('F0006','DISASTER MOVIE', 'Science-fiction',1.9, 'Grosvenor Park', 87, 'Amerika'),
('F0007','HABIBIE DAN AINUN','Drama', 4.2, 'MD Pictures', 121, 'Indonesia'),
('F0008','POLICE STORY','Action',3.1,'Golden Way Films Ltd',120, 'Hongkong'),
('F0009','WARKOP DKI REBORN','Comedy',3.8,'Falcon Pictures',100, 'Indonesia'),
('F0010','FERRARI VS FORD','Drama',4.0,'Chernin Entertainment',152,'Amerika')

select * from tblFilm

create table tblJadwal(
	ID int identity(1,1),
	KodeJadwal varchar(5) primary key not null,
	Tanggal Datetime not null,
	Jam varchar(10) not null,
	KodeFilm varchar(5) not null,
	KodeTeather varchar(5) not null,
)

insert into tblJadwal 
values
('J0001',CAST(N'2022-11-22T13:30:00.000' AS DateTime),'13.30','F0001','T0001'),
('J0002',CAST(N'2022-11-23T13:30:00.000' AS DateTime),'13.30','F0001','T0002'),
('J0003',CAST(N'2022-11-22T16:05:00.000' AS DateTime),'16.05','F0002','T0001'),
('J0004',CAST(N'2022-11-22T00:13:00.000' AS DateTime),'13.00','F0002','T0003'),
('J0005',CAST(N'2022-11-23T00:13:00.000' AS DateTime),'13.00','F0008','T0004')


create table tblKursi(
	ID int identity(1,1),
	KodeKursi varchar(5) primary key not null,
	KodeTeather varchar(5) not null,
	Nama varchar(50) not null,
)

insert into tblKursi
values
('K0001', 'T0001', 'A1'),
('K0002', 'T0001', 'A2'),
('K0003', 'T0002', 'A3'),
('K0004', 'T0004', 'B5'),
('K0005', 'T0004', 'C2'),
('K0006', 'T0005', 'C3')

create table tblTeather(
ID int identity(1,1),
KodeTeather varchar(5) primary key not null,
Nama varchar(30) not null
)

insert into tblTeather
values
('T0001', 'Theater 1'),
('T0002', 'Theater 2'),
('T0003', 'Theater 3'),
('T0004', 'Theater 4'),
('T0005', 'Theater 5')

select * from tblTeather

create table tblTransaksi(
ID int identity(1,1),
KodeTransaksi varchar(5) primary key not null,
KodeJadwal varchar(5) not null,
KodeKursi varchar(5) not null,
Jumlah_Dibayar decimal(18,2) not null
)

insert into tblTransaksi 
values
('R0001','J0001','K0001',50000),
('R0002','J0001', 'K0002',50000),
('R0003','J0002', 'K0003',100000),
('R0004','J0003', 'K0004',55000),
('R0005','J0004', 'K0005',50000)
--------------------------------------------------------------------------------------------------------
-- soal DBBioskop --
--1. 
--2.
--3.
--4.
--5.
--6.
--7.
--8.
--9.
--10.
--11.
--12.
--13.
--14.
--15.