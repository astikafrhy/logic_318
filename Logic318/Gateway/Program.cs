﻿namespace Gateway;
public class Program
{
    static void Main(string[] args)
    {
        Menu();
    }

    static void Menu()
    {
        bool ulangi = true;
       // int input;
        while (ulangi)
        {
            Console.WriteLine("=== Welcome to batch 318 ===");
            Console.WriteLine("|        1. Logic01        |");
            Console.WriteLine("|        2. Logic02        |");
            Console.WriteLine("|        3. Logic03        |");
            Console.WriteLine("|        4. Logic04        |");
            Console.WriteLine("|        5. Logic05        |");
            Console.WriteLine("|        6. Logic06        |");
            Console.WriteLine("|        7. Logic07        |");
            Console.WriteLine("|        8. Logic08        |");
            Console.WriteLine("|        9. Logic09        |");
            Console.WriteLine("|       10. Logic10        |");
            Console.WriteLine();
            Console.Write("Pilih Logic : ");
            int hari = int.Parse(Console.ReadLine());

            switch (hari)
            {
                case 5:
                    Logic05.Program program05 = new Logic05.Program();
                    kembali();
                    break;

                case 6:
                    Logic06.Program program06 = new Logic06.Program();
                    kembali();
                    break;
                case 7:
                    Logic07.Program program07 = new Logic07.Program();
                    kembali();
                    break;
                case 8:
                    Logic08.Program program08 = new Logic08.Program();
                    kembali();
                    break;
                case 9:
                    Logic09.Program program09 = new Logic09.Program();
                    kembali();
                    break;
                case 10:
                    Logic10.Program program10 = new Logic10.Program();
                    kembali();
                    break;
                default:
                    Console.WriteLine("Tidak ada dalam menu");
                    break;
            }
        }
        ulangi = false;
    }
    static void kembali()
    {
        Console.WriteLine("\nApakah ingin kembali ke menu utama ? (y/n) ");
        string menuInput = Console.ReadLine().ToLower();
        if (menuInput == "y")
        {
            Console.Clear();
            Menu();
        }
        else
        {
            Console.WriteLine("Terima kasih!");
            Console.WriteLine("Silahkan tekan Enter kembali...");
        }
    }
}