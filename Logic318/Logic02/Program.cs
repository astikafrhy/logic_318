﻿using Logic02;

//switchCase();
//perulanganWhile();
//perulanganDoWhile();
//perulanganForIncrement();
//perulanganForDecrement();
//perulanganFor();
//arrayFor();
//inputArray();
//array2Dimensi();
//array2DimensiFor();
//inisialisasiList();
//listClass();
//listAdd();
//listRemove();
//convertArrayAll();
//padLeft();
//contain();




Console.ReadKey();


static void switchCase()
{
    Console.WriteLine("=== SWITCH CASE ===");
    Console.Write("Pilih buah yang anda inginkan ? (apel/jeruk/pisang) : ");
    string buah = Console.ReadLine().ToLower();

    switch (buah)
    {
        case "apel":
            Console.WriteLine("Buah pilihan anda adalah Apel");
            break;
        case "jeruk":
            Console.WriteLine("Buah pilihan anda adalah Jeruk");
            break ;
        case "pisang":
            Console.WriteLine("Buah pilihan anda adalah Pisang");
            break;
        default:
            Console.WriteLine("Buah pilihan anda tidak tersedia");
            break;
    }

}

static void perulanganWhile()
{
    Console.WriteLine("==== PERULANGAN WHILE ====");
    bool ulangi = true;
    int temp;
    int nilai = 1;

    while (ulangi)
    {
        Console.WriteLine($"proses ke : {nilai}");
        temp = nilai;
        nilai++;

        Console.Write("apakah anda akan mengulangi proses ? (ya/tidak) : ");
        string input = Console.ReadLine().ToLower();

        if(input != "ya" && input != "tidak")
        {
            nilai = temp; 
        }
        else if (input == "tidak")
        {
            ulangi = false;
        }
        else
        {
            nilai = 1;
            Console.WriteLine("Input yang anda masukkan salah");
            ulangi = true;
        }
    }
}

static void perulanganDoWhile()
{
    int a = 0;
    do
    {
        Console.WriteLine(a);
        a++;
    } while (a < 5); 
}

static void perulanganForIncrement()
{
    for(int i = 0; i < 10; i++)
    {
        Console.WriteLine(i);
    }
}

static void perulanganForDecrement()
{
    for (int i = 10; i > 0; i--)
    {
        Console.WriteLine(i);
    }
}

static void perulanganFor()
{
    for(int i = 0; i < 3; i++)
    {
        for( int j = 0; j < 3; j++)
        {
            Console.Write($"({i},{j})");
        }

        Console.Write("\n");
    }
}

static void foreachArray()
{
    int[] namaArray = { 1, 2, 3 };
    string[] stringArray = new string[] {"asti", "isni", "laudry"};
    int[] namaArray3 = new int[3];
    namaArray3[0] = 1;
    namaArray3[1] = 2;
    namaArray3[2] = 3;
     
    int sum = 0;

    foreach( int n in namaArray)
    {
        sum += n;
    }
    Console.WriteLine(sum); 
}

static void arrayFor()
{
    string[] stringArray = new string[] { "asti", "isni", "laudry" };
    for(int i = 0;i < stringArray.Length; i++)
    {
        Console.WriteLine(stringArray[i]);
    }

}

static void inputArray()
{
    int[] a = new int[5];
    for (int i = 0; i < a.Length; i++)
    {
        Console.Write($"Masukkan array ke - {i + 1} = ");
        a[i] = int.Parse(Console.ReadLine());
    }
    foreach(int i in a) { 
        Console.WriteLine(i);
    }
} 

static void array2Dimensi()
{
    int[,] array = new int[,]
    {
        {1, 2, 3 },
        {4, 5, 6},
        {6, 7, 8}
    };
    Console.WriteLine(array[0,2]);
    Console.WriteLine(array[1,1]);
    Console.WriteLine(array[2,1]);
    Console.WriteLine(array[2,2]);
}

static void array2DimensiFor()
{
    int[,] array = new int[,]
    {
        {1, 2, 3},
        {4, 5, 6},
        {6, 7, 8}
    };

    for(int i = 0; i < 3; i++)
    {
        for(int j = 0; j < 3; j++)
        {
            Console.Write(array[i,j] + "\t");
        }
        Console.WriteLine();
    }
}

static void inisialisasiList()
{
    List<string> list = new List<string>()
    {
        "John Doe",
        "Jane Doe",
        "Joe Doe"
    };

    list.Add("James Doe"); 

    Console.WriteLine(string.Join(" , ", list));
}

static void listClass()
{
    Console.WriteLine("=== LIST ===");
    string input = Console.ReadLine();
    int input2 = Console.Read();
    List<User> listUser = new List<User>()
    {
        new User(){Name = "John Doe", Age = 20},
        new User(){Name = "Jane Doe", Age = 25},
        new User(){Name = "Joe Doe", Age = 15}
    };
    for(int i = 0; i < listUser.Count; i++)
    {
        Console.WriteLine(listUser[i].Name + " berumur " + listUser[i].Age + " tahun " );
    }
}

static void listAdd()
{
    Console.WriteLine("=== LIST ADD ===");
    List<User> listUser = new List<User>()
    {
        new User(){Name = "John Doe", Age = 20},
        new User(){Name = "Jane Doe", Age = 25},
        new User(){Name = "Joe Doe", Age = 15}
    };

    User user = new User();
    user.Name = "Jason Doe";
    user.Age = 32;

    listUser.Add(new User() { Name = "James Doe", Age = 21 });
    listUser.Add(user);

    for (int i = 0; i < listUser.Count; i++)
    {
        Console.WriteLine(listUser[i].Name + " berumur " + listUser[i].Age + " tahun ");
    }
}

static void listRemove()
{
    Console.WriteLine("=== LIST REMOVE ===");
    List<User> listUser = new List<User>()
    {
        new User(){Name = "John Doe", Age = 20},
        new User(){Name = "Jane Doe", Age = 25},
        new User(){Name = "Joe Doe", Age = 15}
    };

    listUser.Remove(new User() { Name = "Jane Doe", Age = 21 });
    listUser.RemoveAt(0);

    for (int i = 0; i < listUser.Count; i++)
    {
        Console.WriteLine(listUser[i].Name + " berumur " + listUser[i].Age + " tahun ");
    }
}

static void listInsert()
{
    Console.WriteLine("=== LIST INSERT ===");
    List<User> listUser = new List<User>()
    {
        new User(){Name = "Joe Doe", Age = 15}
    };

    User user = new User();
    user.Name = "Jason Doe";
    user.Age = 32;

    listUser.Insert(1, user);
    listUser.Insert(0, new User() { Name = "Jane Doe", Age = 21 });

    for (int i = 0; i < listUser.Count; i++)
    {
        Console.WriteLine(listUser[i].Name + " berumur " + listUser[i].Age + " tahun ");
    }
}

static void convertArrayAll()
{
    Console.WriteLine("=== CONVERT ARRAY ALL ===");
    Console.Write("Masukkan angka array (pakai koma) : ");
    int[] array = Array.ConvertAll(Console.ReadLine().Split(','), int.Parse);

    Console.WriteLine(string.Join("\t", array));
}

static void padLeft()
{
    Console.WriteLine("=== PAD LEFT ===");
    Console.Write("Masukkan input : ");
    int input = int.Parse(Console.ReadLine());
    Console.Write("Masukkan panjang karakter : ");
    int panjang = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Char : ");
    char karakter = char.Parse(Console.ReadLine());

    Console.WriteLine($"Hasil PadLeft : {input.ToString().PadLeft(panjang,karakter)}");
}

static void contain()
{
    Console.WriteLine("=== CONTAIN ===");
    Console.Write("Masukkam kalimat : ");
    string kalimat = Console.ReadLine();
    Console.Write("Masukkan Contain : ");
    string contain = Console.ReadLine();

    if (kalimat.Contains(contain))
    {
        Console.WriteLine($"Kalimat ({kalimat}) ini mengandung {contain}");
    }
    else
    {
        Console.WriteLine($"Kalimat ({kalimat}) ini mengandung {contain}");  
    }
}