﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic10
{
    internal class BilanganPrima
    {
        public BilanganPrima()
        {
			Console.Write("Masukkan Batas Bilangan Prima : ");
			int bilangan = int.Parse(Console.ReadLine());

			bool prima = true;


			if (bilangan >= 2)
			{
				for (int i = 2; i <= bilangan; i++)
				{
					for (int j = 2; j < i; j++)
					{
						if ((i % j) == 0)
						{
							prima = false;
							break;
						}
					}
					 
					if (prima)
						Console.WriteLine("Bilangan " + i + " adalah bilangan prima");
					prima = true;
				}
			}
			else
				Console.WriteLine("Tidak ada bilangan prima yang bisa dituliskan");
			Console.ReadLine();
		}

	}
}
