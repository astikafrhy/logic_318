﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic10
{
    internal class SimplifiedChessEngine
    {
        public SimplifiedChessEngine()
        {
            //B1
            Console.WriteLine();
            Console.Write("Queen White : ");
            string qWhite = Console.ReadLine();
            Console.Write("Queen Black : ");
            string qBlack = Console.ReadLine();

            string[,] cBoard = new string[4,4];

            string rowMap = "4321";
            string colMap = "ABCD";


            for (int i = 0; i < cBoard.GetLength(0); i++)
            {
                for (int j = 0; j < cBoard.GetLength(1); j++)
                {
                    if(rowMap.IndexOf(qWhite[1]) == i || colMap.IndexOf(qWhite[0]) == j || 
                        rowMap.IndexOf(qWhite[1]) + j == colMap.IndexOf(qWhite[0]) + i
                        || rowMap.IndexOf(qWhite[1]) + colMap.IndexOf(qWhite[0]) == i + j)
                        cBoard[i,j] = "X";
                    else
                        cBoard[i, j] = ".";
                }
            }
            if (cBoard[rowMap.IndexOf(qBlack[1]), colMap.IndexOf(qBlack[0])] == "X")
            {
                Console.WriteLine();
                Console.WriteLine("YES");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine("NO");
                Console.WriteLine();
            }

            cBoard[rowMap.IndexOf(qWhite[1]), colMap.IndexOf(qWhite[0])] = "QW";
            cBoard[rowMap.IndexOf(qBlack[1]), colMap.IndexOf(qBlack[0])] = "QB";
            
            Printing.array2Dim(cBoard);

           

        }
    }
}
