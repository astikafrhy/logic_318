﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic10
{
    internal class ProgressiveTax
    {
        public ProgressiveTax()
        {
            
            double pajak = 0, min = 0; 
            Console.WriteLine();
            Console.Write("Masukkan Gaji : Rp.");
            double gaji = double.Parse(Console.ReadLine());

            
            if (gaji <= 25000)
            {
                pajak = 0;
                Console.WriteLine($"{gaji} = {pajak}");
            }
            else
            {
                min = gaji - 25000;
                if (min > 0)
                    if (min - 50000 > 0)
                        pajak += 50000 * 0.05;
                    else
                        pajak += min * 0.05;
                if (min > 50000)
                    if (min - 50000 > 50000)
                        pajak += 50000 * 0.1;
                    else
                        pajak += (min - 50000) * 0.1;
                if (min > 100000)
                    if (min - 100000 > 100000)
                        pajak += 100000 * 0.15;
                    else
                        pajak += (min - 100000) * 0.15;
                if (min > 200000)
                    pajak += (min - 200000) * 0.25;
            }
            Console.WriteLine();
            Console.WriteLine(pajak);
           
       
        }
    }
}
