﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class Staircase
    {
        public Staircase()
        {
            Console.Write("Masukkan panjang segitiga: ");
            int panjang = int.Parse(Console.ReadLine());

            for (int row = 0; row < panjang; row++)
            {
                Console.Write("\t\t\t");
                for (int col = 0; col < panjang; col++)
                {
                    //if (col < panjang - 1 - row)
                    //    Console.Write(" ");
                    if (row + col == panjang - 1 || row == panjang - 1 || col == panjang - 1)
                        Console.Write("#");
                    else
                        Console.Write(" ");
                }
                Console.WriteLine();
            }


        }
    }
}
