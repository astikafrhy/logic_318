﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class SolveMeFirst
    {
        public SolveMeFirst()
        {
            //int hasil = 0;
            Console.WriteLine();
            Console.Write("Masukkan a : ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Masukkan b : ");
            int b = int.Parse(Console.ReadLine());
            Console.WriteLine();

            int result = Addition(a, b);
           // hasil = a + b;

            Console.WriteLine($"Hasil = {a} + {b} = {Addition(a, b)}");

        }

        public int Addition (int value1, int value2)
        {
            return value1 + value2;
        }

        public int Addition(int value1, int value2, int value3)
        {
            return value1 + value2 + value3;
        }
    }
}
