﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class SimpleArraySum
    {
        public SimpleArraySum()
        {
            Console.WriteLine();
            Console.WriteLine("=== Simple Array Sum ===");
            //Console.Write("Masukkan n : ");
            //int n = int.Parse(Console.ReadLine());

                Console.Write("Masukkan angka : ");
                string[] angka = Console.ReadLine().Split(' ');
                int[] arr = Array.ConvertAll<string, int>(angka, int.Parse);

                //int[] arr = new int[] { 1, 2, 3 , 4, 10, 11};
                int sum = 0;
                for (int i = 0; i < arr.Length; i++)
                {
                    sum += arr[i];
                }
                Console.WriteLine(sum);
            

        }
    }
}
