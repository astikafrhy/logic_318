﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class VeryBigSum
    {
        public VeryBigSum()
        {
            Console.WriteLine();
            Console.Write("Masukkan deret angka : ");
            string[] angka = Console.ReadLine().Split(' ');
            int[] arr = Array.ConvertAll<string, int>(angka, int.Parse);

            long sum = 0;
            for(int i = 0; i < arr.Length; i++)
            {
                sum += arr[i];
            }
            Console.WriteLine($"{sum}");
        }
    }
}
