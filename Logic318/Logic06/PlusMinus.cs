﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class PlusMinus
    {
        public PlusMinus()
        {
            Console.Write("Masukkan angka: ");
            int[] arrAngka = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            double nol = 0, positif = 0, negatif = 0;

            for (int i = 0; i < arrAngka.Length; i++)
            {
                if (arrAngka[i] == 0)
                    nol++;
                else if (arrAngka[i] > 0)
                    positif++;
                else
                    negatif++;
            }

            double hasilPositif = positif / arrAngka.Length;
            double hasilNegatif = negatif / arrAngka.Length;
            double hasilNol = nol / arrAngka.Length;

            Console.WriteLine(Math.Round(hasilPositif, 6).ToString("n6"));
            Console.WriteLine(Math.Round(hasilNegatif, 6).ToString("n6"));
            Console.WriteLine(Math.Round(hasilNol, 6).ToString("n6"));

        }
    }
}
