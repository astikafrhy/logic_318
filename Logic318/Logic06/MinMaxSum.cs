﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class MinMaxSum
    {
        public MinMaxSum()
        {
            Console.WriteLine();
            Console.Write("Masukkan deret angka : ");
            string[] angka = Console.ReadLine().Split(' ');
            int[] arr = Array.ConvertAll<string, int>(angka, int.Parse);

            int sum = 0;
            int min = arr.Min();
            int max = arr.Max();
            for (int i = 0; i < arr.Length; i++)
            {
                sum = sum + arr[i];
            }
            Console.WriteLine();
            Console.WriteLine($" Penjumlahan terkecil = {sum - max}");
            Console.WriteLine($" Penjumlahan terbesar = {sum - min}");
        }
    }

}
