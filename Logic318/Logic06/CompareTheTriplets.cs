﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class CompareTheTriplets
    {
        public CompareTheTriplets()
        {
            Console.WriteLine();
            Console.Write("Masukkan nilai alice : ");
            int[] arralice = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            Console.Write("Masukkan nilai bob   : ");
            int[] arrbob = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int alice = 0,  bob = 0;
            for(int i = 0; i < arralice.Length; i++)
            {
                if(arralice[i] > arrbob[i])
                    alice++;
                else if (arralice[i] < arrbob[i])
                    bob++;
            }
            Console.WriteLine();
            Console.WriteLine($"Skor alice : {alice}");
            Console.WriteLine($"Skor bob   : {bob}");


        }
    }
}
