﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic06
{
    internal class DiagonalDifference
    {
        public DiagonalDifference()
        {
            Console.WriteLine();
            Console.WriteLine("=== Diagonal Difference ===");
            Console.Write("Masukkan n : ");
            int n = int.Parse(Console.ReadLine());

            int[,] matrix = new int[n,n];
            int mainDiagonal = 0, subDiagonal = 0;

            for(int i = 0; i < n; i++)
            {
                int[] arrayInput = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
                for(int j = 0; j < n ; j++)
                {
                    if(i == j)
                    {
                        mainDiagonal += arrayInput[j];
                    }
                    if(i + j == n -1)
                    {
                        subDiagonal += arrayInput[j];
                    }
                    matrix[i,j] = arrayInput[j];
                }
            }
            int diagonalDiff = Math.Abs(mainDiagonal - subDiagonal);
            Printing.array2Dim(matrix);

            Console.WriteLine($"Diagonal Difference = {diagonalDiff}");
        }
    }
}
