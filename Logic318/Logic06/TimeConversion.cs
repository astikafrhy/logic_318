﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic06
{
    internal class TimeConversion
    {
        public TimeConversion()
        {
            Console.Write("Masukkan waktu (HH:mm:ss AM/PM): ");
            string waktu = Console.ReadLine();

            int jam = int.Parse(waktu.Substring(0, 2));
            //int menit = int.Parse(waktu.Substring(3,2));
            string jenisWaktu = waktu.Substring(8);

            if (jam <= 12)
            {
                if (jenisWaktu == "PM")
                {
                    jam += 12;
                    Console.WriteLine(jam.ToString() + waktu.Substring(2, 6));
                }
                else
                {
                    Console.WriteLine(waktu.Substring(0, 8));
                }
            }    
        }
    }
}
