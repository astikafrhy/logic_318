﻿namespace Logic06
{
    public class Program
    {
        public Program()
        {
            Menu();
        }
        static void Main(string[] args)
        {
            Menu();
        }
        private static void Menu()
        {
            Console.WriteLine();
            Console.WriteLine("=== Welcome to Day 06 ===");
            Console.WriteLine("| 1. Solve Me First      |");
            Console.WriteLine("| 2. Time Conversion     |");
            Console.WriteLine("| 3. Simple Array Sum    |");
            Console.WriteLine("| 4. Diagonal Difference |");
            Console.WriteLine("| 5. Plus Minus          |");
            Console.WriteLine("| 6. Staircase           |");
            Console.WriteLine("| 7. Min Max Sum         |");
            Console.WriteLine("| 8. Birthday Cake Candle|");
            Console.WriteLine("| 9. A Very Big Sum      |");
            Console.WriteLine("|10. Compare The Triplets|");

            Console.WriteLine();
            Console.Write("Masukkan no soal : ");
            int soal = int.Parse(Console.ReadLine());

            string answer = "t";
            while (answer.ToLower() == "t")
            {
                switch (soal)
                {
                    case 1:
                        SolveMeFirst soal1 = new SolveMeFirst();
                        kembali();
                        break;
                    case 2:
                        TimeConversion soal2 = new TimeConversion();
                        kembali();
                        break;
                    case 3:
                        SimpleArraySum soal3 = new SimpleArraySum();
                        kembali();
                        break;
                    case 4:
                        DiagonalDifference soal4 = new DiagonalDifference();
                        kembali();
                        break;
                    case 5:
                         PlusMinus soal5 = new PlusMinus();
                        kembali();
                        break;
                    case 6:
                        Staircase soal6 = new Staircase();
                        kembali();
                        break;
                    case 7:
                        MinMaxSum soal7 = new MinMaxSum();
                        kembali();
                        break;
                    case 8:
                        BirthdaycakeCandle soal8 = new BirthdaycakeCandle();
                        kembali();
                        break;
                    case 9:
                        VeryBigSum soal9 = new VeryBigSum();
                        kembali();
                        break;
                    case 10:
                        CompareTheTriplets soal10 = new CompareTheTriplets();
                        kembali();
                        break;
                    default:
                        break;
                }
                Console.WriteLine();
                Console.Write("Press any key...");
                Console.WriteLine();
                Console.ReadKey();
            }
            static void kembali()
            {
                Console.Write("\nApakah ingin kembali ke menu ? (y/n) ");
                string menuInput = Console.ReadLine().ToLower();
                if (menuInput == "y")
                {
                    Console.Clear();
                    Menu();
                }
                else
                {
                    Console.WriteLine("Terima kasih!");
                    Console.WriteLine("Silahkan tekan Enter kembali...");
                }
            }
        }
    }
}
