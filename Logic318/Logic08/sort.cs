﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic08
{
    internal class sort
    {
        public sort()
        {
            Console.WriteLine();
            int[] randomArr = new int[] { 5, 4, 3, 2, 1 };
            Utility.Printing.array1Dim(Array.ConvertAll(randomArr, x => x.ToString()));
            Console.WriteLine();
            int[] sortedArr = Utility.Sorting.Insertion(randomArr);
            Utility.Printing.array1Dim(Array.ConvertAll(sortedArr, x => x.ToString()));

            Console.Write("Press any key...");
            Console.ReadKey();
        }
    }
}
