﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic08
{
    internal class FindTheMedian
    {
        public FindTheMedian()
        {
            Console.WriteLine();
            Console.Write("Masukkan angka : ");
            string[] angka = Console.ReadLine().Split(" ");
            int[] array1 = Array.ConvertAll<String, int>(angka, int.Parse);

            array1 = Sorting.Insertion(array1);

            foreach (int i in array1)
            {
                Console.Write(i);
            }
            Console.WriteLine();

            if(array1.Length % 2 == 0)
            {
                float n1 = array1[array1.Length / 2];
                float n2 = array1[array1.Length / 2 - 1];
                Console.Write((n1 + n2)/ 2);
            }
            else
            {
                Console.WriteLine(array1[array1.Length / 2]);
            }
        }
    }
}
