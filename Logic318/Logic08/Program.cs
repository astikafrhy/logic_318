﻿namespace Logic08
{
    public class Program
    {
        public Program()
        {
            Menu();
        }
        static void Main(string[] args)
        {
            Menu();
        }
        private static void Menu()
        {
            Console.WriteLine();
            Console.WriteLine("=== Welcome to Day 08 ===");
            Console.WriteLine("| 1. Contoh Sorting      |");
            //Console.WriteLine("| 2. Strong Password     |");
            //Console.WriteLine("| 3. Caesar Cipher       |");
            //Console.WriteLine("| 4. Mars Exploration    |");
            //Console.WriteLine("| 5. HackerRank is String|");
            Console.WriteLine("| 6. Counting Sort 2     |");
            Console.WriteLine("| 7. Find The Median     |");
            //Console.WriteLine("| 8. Gamestone           |");
            //Console.WriteLine("| 9. Making Anagram      |");
            //Console.WriteLine("| 10. Two String         |");

            Console.WriteLine();
            Console.Write("Masukkan no soal : ");
            int soal = int.Parse(Console.ReadLine());

            string answer = "t";
            while (answer.ToLower() == "t")
            {
                switch (soal)
                {
                    case 1:
                        sort soal1 = new sort();
                        kembali();
                        break;
                    //case 2:
                    //    StrongPassword soal2 = new StrongPassword();
                    //    kembali();
                    //    break;
                    //case 3:
                    //    CaesarCipher soal3 = new CaesarCipher();
                    //    kembali();
                    //    break;
                    //case 4:
                    //    MarsExploration soal4 = new MarsExploration();
                    //    kembali();
                    //    break;
                    //case 5:
                    //    HackerRankIsString soal5 = new HackerRankIsString();
                    //    kembali();
                    //    break;
                    case 6:
                        CountingSort2 soal6 = new CountingSort2();
                        kembali();
                        break;
                    case 7:
                        FindTheMedian soal7 = new FindTheMedian();
                        kembali();
                        break;
                    //case 8:
                    //    Gamestone soal8 = new Gamestone();
                    //    kembali();
                    //    break;
                    //case 9:
                    //    MakingAnagram soal9 = new MakingAnagram();
                    //    kembali();
                    //    break;
                    //case 10:
                    //    TwoString soal10 = new TwoString();
                    //    kembali();
                    //    break;
                    default:
                        break;
                }
                Console.WriteLine();
                Console.Write("Press any key...");
                Console.WriteLine();
                Console.ReadKey();
            }
            static void kembali()
            {
                Console.Write("\nApakah ingin kembali ke menu ? (y/n) ");
                string menuInput = Console.ReadLine().ToLower();
                if (menuInput == "y")
                {
                    Console.Clear();
                    Menu();
                }
                else
                {
                    Console.WriteLine("Terima kasih!");
                    Console.WriteLine("Silahkan tekan Enter kembali...");
                }
            }
        }
    }
}
