﻿//soal1();
//soal2();
soal3();
//soal4();
//soal5();
//soal6();
//soal7();
//soal8();
 
Console.ReadKey();

static void soal1()
{
    Console.WriteLine("=== NILAI MAHASISWA ===");
    Console.Write("Masukkan Nilai Mahasiswa : ");
    string input = Console.ReadLine().Replace(" ","");
    //string grade = "";

    if( input == "")
    {
        Console.WriteLine("TIDAK BOLEH KOSONG!");
    }
    else {
        
        int nilai=int.Parse(input);

        if(nilai >= 0 && nilai <= 49)
        {
            Console.WriteLine("Grade E");
        }
        else if (nilai >=50 && nilai <= 69)
        {
            Console.WriteLine("Grade C");
        }
        else if (nilai >= 70 && nilai <= 89)
        {
            Console.WriteLine("Grade B");
        }
        else if (nilai >= 90 && nilai <= 100)
        {
            Console.WriteLine("Grade A");
        }
        else
        {
            Console.WriteLine("Nilai yang anda masukkan salah");
        }
    }
}

static void soal2()
{
    int poin = 0;
    Console.WriteLine("=== PULSA ===");
    Console.Write("Masukkan Pulsa : ");
    int pulsa = int.Parse(Console.ReadLine());
    
    if(pulsa >= 10000 && pulsa <= 24999)
    {
        Console.WriteLine("Mendapatkan point sebesar = 80");
    }
    else if(pulsa >= 25000 && pulsa <= 49999)
    {
        Console.WriteLine("Mendapatkan point sebesar = 200");
    }
    else if(pulsa >= 50000 && pulsa <= 99999)
    {
        Console.WriteLine("Mendapatkan point sebesar = 400");
    }
    else if (pulsa >= 100000)
    {
        Console.WriteLine("Mendapatkan point sebesar = 800");
    }
    else
    {
        Console.WriteLine("pulsa yang anda masukkan salah");
    }
}

static void soal3()
{
    int ongkir = 5000;
    double belanja = 0, jarak = 0;
    double totalBelanja = 0, totalOngkir = 0, totalDiskon = 0;
    double diskon = 0.4;
    while (true)
    {
        Console.WriteLine("=== GRABFOOD ===");
        Console.Write("Belanja        = ");
        belanja = double.Parse(Console.ReadLine());
        Console.Write("jarak          = ");
        jarak = double.Parse(Console.ReadLine().Replace('.', ','));
        if (belanja < 0)
            Console.WriteLine("Inputan salah");
        else if (jarak < 0)
            Console.WriteLine("inputan salah");
        else
            break;
    }
    Console.Write("Masukkan Promo = ");
    string kodePromo = Console.ReadLine().ToUpper();

    if (belanja >= 30000 && kodePromo == "JKTOVO")
    {
        if (jarak >= 5)
        {
            totalOngkir = ongkir + ((jarak - 5) * 1000);
        }
        else
        {
            totalOngkir = ongkir;
        }

        totalDiskon = belanja * diskon;
        totalBelanja = belanja - totalDiskon + totalOngkir;
        Console.WriteLine("===========================");
        Console.WriteLine($"Belanja        : {belanja}");
        Console.WriteLine($"Diskon 40%     : {totalDiskon}");
        Console.WriteLine($"Ongkir         : {totalOngkir}");
        Console.WriteLine($"Total Belanja  : {totalBelanja}");
    }
    else
    {
        if (jarak <= 5)
        {
            totalOngkir = ongkir + ((jarak - 5) * 1000);
        }
        else
        {
            totalOngkir = ongkir;
        }
        totalBelanja = belanja + totalOngkir;

        Console.WriteLine("===========================");
        Console.WriteLine($"Belanja        : {belanja}");
        Console.WriteLine($"Diskon 40%     : 0");
        Console.WriteLine($"Ongkir         : {totalOngkir}");
        Console.WriteLine($"Total Belanja  : {totalBelanja}");
    }
}

static void soal4()
{
    int belanja;
    int ongkosKrm, plhVoucher;
    int diskonOngkir, diskonBelanja, totalBelanja;

    Console.WriteLine("==== SOPI ====");
    Console.Write("Belanja :");
    belanja = int.Parse(Console.ReadLine());
    Console.Write("Ongkos Kirim :");
    ongkosKrm = int.Parse(Console.ReadLine());
    Console.Write("Pilih Voucher :");
    plhVoucher = int.Parse(Console.ReadLine());

    switch (plhVoucher)
    {
        case 1:
            if (belanja >= 30000)
            {
                diskonOngkir = 5000;
                diskonBelanja = 5000;
                totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
            }
            else
            {
                diskonOngkir = 0;
                diskonBelanja = 0;
                totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
            }
            Console.WriteLine("=========================");
            Console.WriteLine($"Belanja        : {belanja}");
            Console.WriteLine($"Ongkos Kirim   : {ongkosKrm}");
            Console.WriteLine($"Diskon Ongkir  : {diskonOngkir}");
            Console.WriteLine($"Diskon Belanja : {diskonBelanja}");
            Console.WriteLine($"Total Belanja  : {totalBelanja}");
            break;
        case 2:
            if (belanja >= 50000)
            {
                diskonOngkir = 10000;
                diskonBelanja = 10000;
                totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
            }
            else
            {
                diskonOngkir = 0;
                diskonBelanja = 0;
                totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
            }
            Console.WriteLine("=========================");
            Console.WriteLine($"Belanja        : {belanja}");
            Console.WriteLine($"Ongkos Kirim   : {ongkosKrm}");
            Console.WriteLine($"Diskon Ongkir  : {diskonOngkir}");
            Console.WriteLine($"Diskon Belanja : {diskonBelanja}");
            Console.WriteLine($"Total Belanja  : {totalBelanja}");
            break;
        case 3:
            if (belanja >= 100000)
            {
                diskonOngkir = 20000;
                diskonBelanja = 20000;
                totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
            }
            else
            {
                diskonOngkir = 0;
                diskonBelanja = 0;
                totalBelanja = belanja + ongkosKrm - diskonOngkir - diskonBelanja;
            }
            Console.WriteLine("=========================");
            Console.WriteLine($"Belanja        : {belanja}");
            Console.WriteLine($"Ongkos Kirim   : {ongkosKrm}");
            Console.WriteLine($"Diskon Ongkir  : {diskonOngkir}");
            Console.WriteLine($"Diskon Belanja : {diskonBelanja}");
            Console.WriteLine($"Total Belanja  : {totalBelanja}");
            break;
        default:
            Console.WriteLine("=========================");
            Console.WriteLine("voucher tidak tersedia!");
            break;
    }

}

static void soal5()
{
    Console.WriteLine("=== GENERASI ===");
    Console.Write("Masukkan Nama Anda : ");
    string nama = Console.ReadLine();
    Console.Write("Tahun berapa anda lahir ? ");
    int tahun = int.Parse(Console.ReadLine());

    if(tahun >= 1944 && tahun <= 1964)
    {
        Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Baby Boomer");
    }
    else if (tahun >= 1965 && tahun <= 1979)
    {
        Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi X");
    }
    else if (tahun >= 1980 && tahun <= 1994)
    {
        Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Y (Milenial)");
    }
    else if (tahun >= 1995 && tahun <= 2015)
    {
        Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong Generasi Z");
    }
    else
    {
        Console.WriteLine($"{nama}, tahun yang anda masukan salah");
    }
}

static void soal6()
{
    double pajak = 0, bpjs = 0, gaji = 0, totalgaji = 0, totalpajak = 0, totalgapok = 0; ;
    Console.WriteLine("=== GAJI ===");
    Console.Write("Masukkan nama : ");
    string nama = Console.ReadLine();
    Console.Write("Tunjangan     : ");
    double tunjangan = double.Parse(Console.ReadLine());
    Console.Write("Gaji Pokok    : ");
    double gapok = double.Parse(Console.ReadLine());
    Console.Write("Banyak Bulan  : ");
    double bulan = double.Parse(Console.ReadLine());

    

    gaji = (gaji + tunjangan) - (pajak + bpjs);
    totalgaji = (gaji + tunjangan) - (bpjs + pajak) * bulan;

    if (gapok + tunjangan <= 5000000)
    {
        pajak = 0.05; 
    }

    else if (gapok + tunjangan > 5000000 && gapok+tunjangan <= 10000000)
    {
        pajak = 0.1;
    }
    else if (gapok + tunjangan > 10000000)
    {
        pajak = 0.15 ;
    }
    else
    {
        totalgaji = 0;
    }

    totalpajak = pajak * (gapok + tunjangan);
    bpjs = 0.03 * (gapok + tunjangan);
    totalgapok = (gapok + tunjangan) - (bpjs + totalpajak);
    totalgaji = ((gapok + tunjangan) - (bpjs + totalpajak)) * bulan;
    
    Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut : ");
    Console.WriteLine($"Pajak                     = Rp{totalpajak}");
    Console.WriteLine($"bpjs                      = Rp{bpjs}");
    Console.WriteLine($"gaji/bln                  = Rp{totalgapok}");
    Console.WriteLine($"total gaji/ banyak bulan  = Rp{totalgaji}");


}

static void soal7()
{
    double nilai_bmi = 0;
    Console.WriteLine("=== NILAI BMI ===");
    Console.Write("Masukkan berat badan anda (kg) : ");
    double bb = double.Parse(Console.ReadLine());
    Console.Write("Masukkan tinggi badan anda (cm) : ");
    double tb = double.Parse(Console.ReadLine());

    nilai_bmi = bb/Math.Pow((tb/100),2);
    Console.WriteLine("Nilai BMI anda adalah " + nilai_bmi);

   if(nilai_bmi >= 0 && nilai_bmi <= 18.49)
    {
        Console.WriteLine("Anda termasuk berbadan kurus");
    }
    else if (nilai_bmi >= 18.5 && nilai_bmi <= 24.99)
    {
        Console.WriteLine("Anda termasuk berbadan langsing/sehat");
    }
    else if (nilai_bmi >= 25)
    {
        Console.WriteLine("Anda termasuk berbadan gemuk");
    }
    else
    {
        Console.WriteLine("berat badan dan tinggi badan yang anda masukkan salah");
    }
}

static void soal8()
{
    int rata_rata = 0;
    Console.WriteLine("=== NILAI RATA - RATA ===");
    Console.Write("Masukkan Nilai MTK : ");
    int mtk = int.Parse(Console.ReadLine());

    Console.Write("Masukkan Nilai Fisika : ");
    int fisika = int.Parse(Console.ReadLine());

    Console.Write("Masukkan Nilai Kimia : ");
    int kimia = int.Parse(Console.ReadLine());

    rata_rata = (mtk + fisika + kimia) / 3;
    Console.WriteLine("==============================");
    Console.WriteLine("Nilai Rata-rata : " + rata_rata);


    if (rata_rata >= 0 && rata_rata <= 60)
    {
        Console.WriteLine("Maaf");
        Console.WriteLine("Kamu Gagal");
    }
    else if (rata_rata >= 61 && rata_rata <= 100)
    {
        Console.WriteLine("Selamat");
        Console.WriteLine("Kamu Berhasil");
        Console.WriteLine("Kamu Hebat");
    }
    else
    {
        Console.WriteLine("Nilai yang anda masukkan salah");
    }
}
