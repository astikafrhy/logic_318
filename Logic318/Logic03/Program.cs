﻿
//Break();
//Continue();
//stringToCharArray();
//intToCharArray();
//indeksElementArray();
//datetime();
//datetimeParsing();
//datetimeProperties();
timeSpan();


Console.ReadKey();


static void Break()
{
    for(int i = 0; i < 10; i++)
    {
        if ( i == 5)
        {
            break;
        }
        Console.WriteLine();
    }
}

static void Continue()
{
    for (int i = 0;i < 10; i++)
    {
        if( i == 7)
        {
            continue;
        }
        Console.WriteLine(i);
    }
}

static void stringToCharArray()
{
    Console.WriteLine("=== STRING TO CHAR ARRAY ===");
    Console.Write("Masukkan kalimat : ");
    string kalimat = Console.ReadLine();

    char[] array = kalimat.ToCharArray();

    for(int i = 0; i < array.Length; i++)
    {
        Console.WriteLine(array[i]);
    }
}

static void intToCharArray()
{
    Console.WriteLine("=== INTEGER TO CHAR ARRAY ===");
    Console.Write("Masukkan angka (jangan pakai spasi) : ");
    int kalimat = int.Parse(Console.ReadLine());


    char[] array = kalimat.ToString().ToCharArray();

    for (int i = 0; i < array.Length; i++)
    {
        Console.WriteLine(array[i]);
    }
}

static void indeksElementArray()
{
    
    Console.Write("Masukkan item : ");
    int item = int.Parse(Console.ReadLine());

    List<int> list = new List<int>() { 1, 2, 3, 4, 5, 6 };
    int[] array = new int[] { 5, 6, 7, 8, 9, 10 };

    int indexList = list.IndexOf(item);
    int indexArray = Array.IndexOf(array, item);

    if(indexList != -1)
    {
        Console.WriteLine("List element {0} is found at index {1}", item, indexList);
    }
    else
    {
        Console.WriteLine("Element not found in the given List");
    }
    if (indexArray != -1)
    {
        Console.WriteLine("List element {0} is found at index {1}", item, indexArray);
    }
    else
    {
        Console.WriteLine("Element not found in the given Array");
    }
}

static void datetime()
{
    DateTime dt1 = new DateTime();
    Console.WriteLine(dt1);

    DateTime dt2 = DateTime.Now;
    Console.WriteLine(dt2);

    DateTime dt3 = new DateTime(2023, 5, 4);
    Console.WriteLine(dt3);

    DateTime dt4 = new DateTime(2023, 5, 4, 11, 55, 55);
    Console.WriteLine(dt4);
}

static void datetimeParsing()
{
    Console.WriteLine("=== DATETIME PARSING ===");
    Console.Write("Masukkan tanggal (MM/dd/yyyy) : ");
    string dateString = Console.ReadLine(); //"06/30/2023"

    try
    {
        DateTime dt1 = DateTime.ParseExact(dateString,"MM/dd/yyyy",null);
        Console.WriteLine(dt1);
    }
    catch (Exception ex)
    {
        Console.WriteLine("Format yang anda masukkan salah!");
        Console.WriteLine("Pesan error : " + ex.Message);
    }
}

static void datetimeProperties()
{
    Console.WriteLine("=== DATETIME PROPERTIES ===");
    DateTime date = new DateTime(2023,5,4, 11, 1,55);

    int tahun = date.Year;
    int bulan = date.Month;
    int hari = date.Day;
    int jam = date.Hour;
    int menit = date.Minute;
    int detik = date.Second;
    int weekDay = (int)date.DayOfWeek;

    string haristring = date.ToString("dddd");
    string haristring2 = date.DayOfWeek.ToString("dddd");

    Console.WriteLine($"Tahun : {tahun}");
    Console.WriteLine($"Bulan : {bulan}");
    Console.WriteLine($"hari : {hari}");
    Console.WriteLine($"Jam : {jam}"); 
    Console.WriteLine($"menit : {menit}");
    Console.WriteLine($"detik : {detik}");
    Console.WriteLine($"weekDay : {weekDay}");
    Console.WriteLine($"hariString : {haristring}");
    Console.WriteLine($"hariString2 : {haristring2}");
}

static void timeSpan()
{
    Console.WriteLine("=== TIME SPAN ===");
    DateTime date1 = new DateTime(2023, 5, 4, 11, 35, 45);
    DateTime date2 = new DateTime(2023, 6, 14, 12, 45, 55);

    TimeSpan interval = date2 - date1;

    Console.WriteLine("Hari             : "+ interval.Days);
    Console.WriteLine("Total  Hari      : "+ interval.TotalDays);
    Console.WriteLine("Jam              : "+ interval.Hours);
    Console.WriteLine("Total Jam        : "+ interval.TotalHours);
    Console.WriteLine("Menit            : "+ interval.Minutes);
    Console.WriteLine("Total Menit      : "+ interval.TotalMinutes);
    Console.WriteLine("Detik            : "+ interval.Hours);
    Console.WriteLine("Total Detik      : "+ interval.TotalHours);
    Console.WriteLine("Mili Detik       : "+ interval.Minutes);
    Console.WriteLine("Total Mili Detik : "+ interval.TotalMinutes);
    Console.WriteLine("Ticks            : "+ interval.Ticks);  
}