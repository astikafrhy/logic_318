﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic04
{
    public class Mamalia
    {
        public virtual void pindah()
        {
            Console.WriteLine("Kucing Lari...");
        }
    }

    public class Kucing : Mamalia
    {
       
    }

    public class Paus : Mamalia
    {
        public override void pindah()
        {
            Console.WriteLine("Paus Berenang...");

        }
    }
}
