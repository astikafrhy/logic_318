﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic04
{
    public abstract class calculatorAbstract
    {
                                   //parameter 1(int x) dan ke 2 (int y)
                                   //fungsi ( jumlah dan kurang)
        public abstract int jumlah(int x, int y);
        public abstract int kurang (int x, int y);
    }

    public class TestTurunan : calculatorAbstract
    {
        public override int jumlah(int x, int y)
        {
            return x + y;
        }

        public override int kurang(int x, int y)
        {
            return x - y;
        }
    }
}
