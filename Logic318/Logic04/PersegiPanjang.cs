﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic04
{
    public class PersegiPanjang
    {
        public double panjang;
        public double lebar;
        public double luasPersegiPanjang()
        {
            return panjang * lebar;
        }

        public void tampilkanData()
        {
            Console.WriteLine($"Luas : {luasPersegiPanjang}");
        }
    }
}
