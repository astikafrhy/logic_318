﻿using Logic04;

//abstractClass();
//objectClass();
//constructor();
//encapsulation();
//inheritance();
//overiding();
//returnTypePenjumlahan();
//rekursifDescending();
rekursifAscending();

Console.ReadKey();

static void rekursifAscending()
{
    Console.WriteLine("=== REKURSIF  ASC FUNCTION===");
    Console.Write("Maukkan  input : ");
    int input = int.Parse(Console.ReadLine());


    int start = 1;
    perulanganAsc(input,start);
}

static int perulanganAsc(int input, int start)
{
    if (input == start)
    {
        Console.WriteLine($"{start}");
        return input;
    }
    Console.WriteLine($"{start}");
    return perulanganAsc(input, start + 1);
}

static void rekursifDescending()
{
    Console.WriteLine("=== REKURSIF DESC FUNCTION===");
    Console.Write("Maukkan input : ");
    int input = int.Parse(Console.ReadLine());

    perulangan(input);

}

static int perulangan(int input)
{
    if(input == 1)
    {
        Console.WriteLine($"{input}");
        return input;
    }
    Console.WriteLine($"{input}");
    return perulangan(input - 1);
}

static void returnTypePenjumlahan()
{
    Console.WriteLine("=== METHOD RETURN TYPE ===");
    Console.Write("Jumlah Mangga = ");
    int mangga = int.Parse(Console.ReadLine());
    Console.Write("Jumlah Apel = ");
    int apel = int.Parse(Console.ReadLine());

    int hasil = hitung(mangga,apel);

    Console.WriteLine($"Jumlah Mangga + Apel = {hasil}");
}

static int hitung(int mangga, int apel)
{
    return mangga + apel;
}


static void overiding()
{
    Console.WriteLine("=== OVERIDING ===");
    Kucing kucing = new Kucing();
    Paus paus = new Paus();

    kucing.pindah();
    paus.pindah();

    //Console.WriteLine($"Kucing {kucing.pindah}");
    //Console.WriteLine($"Paus   {paus.pindah}");
}

static void inheritance()
{
    Console.WriteLine("=== INHERITANCE ===");
    TypeMobil tm = new TypeMobil();
    tm.Civic();
}

static void encapsulation()
{
    Console.WriteLine("=== ENCAPSULATION ===");
    PersegiPanjang pp = new PersegiPanjang();
    pp.panjang = 10;
    pp.lebar = 20;
    pp.tampilkanData();
}

static void constructor()
{
    Console.WriteLine("=== CONSTRUCTOR ===");
    Mobil mobil = new Mobil("B 1234 K");
    string platno = mobil.getPlatNo();

    Console.WriteLine($"Mobil ini bernomor = {platno}");
}


static void abstractClass()
{
    Console.WriteLine("=== ABSTRACT CLASS ===");
    Console.Write("Masukkan input x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukkan input y: ");
    int y = int.Parse(Console.ReadLine());

    TestTurunan cal = new TestTurunan();
    int jumlah = cal.jumlah(x, y);
    int kurang = cal.kurang(x, y);
    Console.WriteLine("========================");
    Console.WriteLine($"Hasil {x} + {y} = {jumlah}");
    Console.WriteLine($"Hasil {x} - {y} = {kurang}");
}

static void objectClass()
{
    Console.WriteLine("=== OBJECT CLASS ===");
    Mobil mobil = new Mobil("R1") {nama ="Ferrari", kecepatan = 0, bensin = 10, posisi = 0}; //cara1

    //Mobil mobil2 = new Mobil(); //cara2
    //mobil2.nama = "Hyundai";
    //mobil2.kecepatan = 0;
    //mobil2.bensin = 10;
    //mobil2.posisi = 0;

    mobil.percepat();
    mobil.maju();
    mobil.isiBensin(20);

    mobil.utama();
} 