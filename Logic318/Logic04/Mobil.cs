﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic04
{
    public class Mobil
    {
        public void utama()
        {
            Console.WriteLine($"Nama      = {nama}");
            Console.WriteLine($"Kecepatan = {kecepatan}");
            Console.WriteLine($"Bensin    = {bensin}");
            Console.WriteLine($"posisi    = {posisi}");
        }

        public double kecepatan;
        public double bensin;
        public double posisi;
        public string nama;
        public string platNo;

        public Mobil(string _platNo)
        {
            platNo = _platNo;
        }

        public Mobil() { }

        public void percepat()
        {
            this.kecepatan += 10;
            this.bensin -= 5;
        }

        public void maju()
        {
            this.posisi += this.kecepatan;
            this.bensin -= 2;
        }

        public void isiBensin(double bensin)
        {
            this.bensin += bensin;

        }

        public string getPlatNo()
        {
            return platNo;
        }
    }
}
