﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
    internal class Soal11
    {
        public Soal11()
        {
            Console.Write("Masukkan n : ");
            int n = int.Parse(Console.ReadLine());
            string[] arrString = new string[n];
            //  1   3   5   7   9   11  13
            for (int i = 0; i < n; i++)
            {
                arrString[i] = i.ToString();
            }
            Printing.array1Dim(arrString);
        }
    }
}
