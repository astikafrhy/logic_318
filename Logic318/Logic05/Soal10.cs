﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
    internal class Soal10
    {
        public Soal10()
        {
            //3	9	27	XXX	243	729	2187
            Console.Write("Masukkan banyak inputan (N) : ");
            int n = int.Parse(Console.ReadLine());

            string[] arrString = new string[n];


            //int angka = 3;
            //for (int i = 1; i <= n; i++)
            //{
            //    if (i % 4 == 0)
            //        Console.Write("XXX \t");
            //    else
            //        Console.Write($"{angka} \t");
            //    angka *= 3;
            //}
            //Console.WriteLine();

            //cara2
            for (int i = 1; i <= n; i++)
            {
                Console.Write(i != 4 ? $"{Math.Pow(3, i)}\t" : $"{digitToString(Math.Pow(3, i))}\t");
            }
            Printing.array1Dim(arrString);

        }

        private string digitToString(double digit)
        {
            string result = "";
            for(int i = 0; i < digit.ToString().Length; i++)
            {
                result += "X";
            }

            return result;
        }
    }  
}
