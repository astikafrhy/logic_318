﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
    internal class Soal09
    {
		public Soal09()
		{
			//1	 5	*	9	13	*	17
			Console.Write("Masukkan banyak inputan (N) : ");
			int n = int.Parse(Console.ReadLine()); ;
            int angka = 4;

			string[] arrString = new string[n];


            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                    Console.Write("* \t");
                else
                    Console.Write(angka + "\t");
                angka *= 4;
            }

            //cara2
            //for (int i = 1; i <= jumlah; i++)
            //{
            //	Console.Write(i % 3 != 0 ? $"{i * 4 - 4}\t" : "*\t");
            //}
            Printing.array1Dim(arrString);

		}
	}
}
