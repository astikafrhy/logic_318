﻿namespace Logic05
{
    public class Program
    {
        public Program()
        {
            Menu();
        }    
        static void Main(string[] args)
        {
            Menu();
        }

        private static void Menu()
        {
            Console.WriteLine("==== Welcome to Day 05 ====");
            Console.WriteLine("|   1. Soal01        	 |");
            Console.WriteLine("|   2. Soal02             |");
            Console.WriteLine("|   3. Soal03		     |");
            Console.WriteLine("|   4. Soal04             |");
            Console.WriteLine("|   5. Soal05	         |");
            Console.WriteLine("|   6. Soal06	         |");
            Console.WriteLine("|   7. Soal07	         |");
            Console.WriteLine("|   8. Soal08	         |");
            Console.WriteLine("|   9. Soal09	         |");
            Console.WriteLine("|   10. Soal10	         |");
            Console.WriteLine("|   11. Soal Array 01	 |");
            Console.WriteLine("|   12. Array 2D Soal 1	 |");
            Console.WriteLine("|   13. Array 2D Soal 2	 |");
            Console.WriteLine("|   14. Array 2D Soal 3	 |");
            Console.WriteLine("|   15. Array 2D Soal 4	 |");
            Console.WriteLine("|   16. Array 2D Soal 5	 |");
            Console.WriteLine("|   17. Array 2D Soal 6	 |");
            Console.WriteLine("|   18. Array 2D Soal 7	 |");
            Console.WriteLine("|   19. Array 2D Soal 8	 |");
            Console.WriteLine("|   20. Array 2D Soal 9	 |");
            Console.WriteLine("|   21. Array 2D Soal 10  |");
            Console.WriteLine("|   0. Exit               |");
            Console.Write("Masukkan no soal : ");
            int soal = int.Parse(Console.ReadLine());

            switch (soal)
            {
                case 1:
                    Soal01 soal1 = new Soal01();
                    kembali();
                    break;
                case 2:
                    Soal02 soal2 = new Soal02();
                    kembali();
                    break;
                case 3:
                    Soal03 soal3 = new Soal03();
                    kembali();
                    break;
                case 4:
                    Soal04 soal4 = new Soal04();
                    kembali();
                    break;
                case 5:
                    Soal05 soal5 = new Soal05();
                    kembali();
                    break;
                case 6:
                    Soal06 soal6 = new Soal06();
                    kembali();
                    break;
                case 7:
                    Soal07 soal7 = new Soal07();
                    kembali();
                    break;
                case 8:
                    Soal08 soal8 = new Soal08();
                    kembali();
                    break;
                case 9:
                    Soal09 soal9 = new Soal09();
                    kembali();
                    break;
                case 10:
                    Soal10 soal10 = new Soal10();
                    break;
                case 11:
                    Soal11 soal11 = new Soal11();
                    kembali();
                    break;
                case 12:
                    Array2DSoal01 soal01array2dim = new Array2DSoal01();
                    kembali();
                    break;
                case 13:
                    Array2DSoal02 soal02array2dim = new Array2DSoal02();
                    kembali();
                    break;
                case 14:
                    Array2DSoal03 soal03array2dim = new Array2DSoal03();
                    kembali();
                    break;
                case 15:
                    Array2DSoal04 soal04 = new Array2DSoal04();
                    kembali();
                    break;
                case 16:
                    Array2DSoal05 soal05 = new Array2DSoal05();
                    kembali();
                    break;
                case 17:
                    Array2DSoal06 soal06 = new Array2DSoal06();
                    kembali();
                    break;
                case 18:
                    Array2DSoal7 soal07array2dim = new Array2DSoal7();
                    kembali();
                    break;
                case 19:
                    Array2DSoal8 soal08array2dim = new Array2DSoal8();
                    kembali();
                    break;
                case 20:
                    Array2DSoal9 soal09array2dim = new Array2DSoal9();
                    kembali();
                    break;
                case 21:
                    Array2DSoal10 soal10array2dim = new Array2DSoal10();
                    kembali();
                    break;
                default:
                    break;
            }
            Console.WriteLine();
            Console.Write("Press any key...");
            Console.ReadKey();
        }

        static void kembali()
        {
            Console.WriteLine("\nApakah ingin kembali ke menu ? (y/n) ");
            string menuInput = Console.ReadLine().ToLower();
            if (menuInput == "y")
            {
                Console.Clear();
                Menu();
            }
            else
            {
                Console.WriteLine("Terima kasih!");
                Console.WriteLine("Silahkan tekan Enter kembali...");
            }
        }

    }

}