﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
    internal class Array2D
    {
    }

    public class Array2DSoal01
    {
        // n = 7 , n2 = 3
        // i : 0    1   2   3   4   5   6
        // 0 : 1    3   9   27  81  243 729

        public Array2DSoal01()
        {
            Console.WriteLine("=== Soal 01 Array 2 Dimensi ===");
            Console.Write("Masukkan nilai n1 : ");
            int n1 = int.Parse(Console.ReadLine());

            Console.WriteLine();

            Console.Write("Masukkan nilai n2 : ");
            int n2 = int.Parse(Console.ReadLine());
                       
            string[,] array = new string[2, n1];

            for(int i = 0; i < n1; i++)
            {
                array[0,i] = i.ToString();
                array[1,i] = Math.Pow(n2,i).ToString();
            }
            Printing.array2Dim(array);
        }
    }

    public class Array2DSoal02
    {
        // n = 7 , n2 = 3
        // i : 0    1   2   3   4    5    6
        // 0 : 1    3  -9  27  81  -243  729
        public Array2DSoal02()
        {
            Console.WriteLine("=== Soal 02 Array 2 Dimensi ===");
            Console.Write("Masukkan nilai n1 : ");
            int n1 = int.Parse(Console.ReadLine());

            Console.WriteLine();

            Console.Write("Masukkan nilai n2 : ");
            int n2 = int.Parse(Console.ReadLine());

            string[,] array = new string[2, n1];

            for (int i = 0; i < n1; i++)
            {
                array[0, i] = i.ToString();
                if((i+1) % 3 == 0)
                    array[1, i] = (Math.Pow(n2, i) * (-1)).ToString();
                else
                    array[1, i] = Math.Pow(n2, i).ToString();
            }
            Printing.array2Dim(array);
        }
    }

    public class Array2DSoal03
    {
        public Array2DSoal03()
        {
           // int angka = 3;
            Console.WriteLine("=== Soal 03 Array 2 Dimensi ===");
            Console.Write("Masukkan nilai n1 : ");
            int n1 = int.Parse(Console.ReadLine());

            Console.WriteLine();

            Console.Write("Masukkan nilai n2 : ");
            int n2 = int.Parse(Console.ReadLine());

            string[,] array = new string[2, n1];
            int angka = n2;

            for (int i = 0; i < n1; i++)
            {
                array[0, i] = i.ToString();
                if(i <= n1 / 2)
                {
                    array[1,i] = angka.ToString();

                    array[1, n1-1-i] = angka.ToString();
                    angka *= 2;
                }  
            }
            Printing.array2Dim(array);
        }
    }

    public class Array2DSoal04
    {
        public Array2DSoal04()
        {
            //n1 = 7, n2 = 5
            //0     1	2	3	4	5	6
            //1	    5	2	10	3	15	4
            Console.WriteLine("== Soal 04 Array 2D ===");
            Console.WriteLine();
            Console.Write("Masukkan nilai n1: ");
            int n1 = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai n2: ");
            int n2 = int.Parse(Console.ReadLine());
            Console.WriteLine();

            string[,] array = new string[2, n1];
            int angka = 0;
            for (int i = 0; i < n1; i++)
            {
                array[0, i] = i.ToString();
                if (i % 2 == 0)
                {
                    array[1, i] = (angka + 1).ToString();
                    angka++;
                }
                else
                    array[1, i] = (n2 * angka).ToString();
            }
            Printing.array2Dim(array);
        }
    }

    public class Array2DSoal05
    {
        //n = 7
        //i :	0	1	2	3	4	5	6
        //0 :	7	8	9	10	11	12	13
        //1 :	14	15	16	17	18	19	20

        //n = 5
        //0	    1	2	3	4
        //5	    6	7	8	9
        //10	11	12	13	14

        public Array2DSoal05()
        {
            Console.WriteLine("== Soal 05 Array 2D ===");
            Console.WriteLine();
            Console.Write("Masukkan nilai n: ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine();

            string[,] array = new string[3, n];
            for (int i = 0; i < n; i++)
            {
                array[0, i] = i.ToString();
                array[1, i] = (n + i).ToString();
                array[2, i] = (n * 2 + i).ToString();
            }
            Printing.array2Dim(array);
        }
    }

    public class Array2DSoal06
    {
        //n = 7
        //0	1	2	3	4	5	6
        //1	7	49	343	2401	16807	117649
        //1	8	51	346	2405	16812	117655

        //n = 5
        //0	1	2	3	4
        //1	5	25	125	625
        //1	6	27	128	629

        public Array2DSoal06()
        {
            Console.WriteLine("\n== Soal 06 Array 2D");
            Console.Write("Masukkan nilai n: ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine();

            string[,] array = new string[3, n];

            for (int i = 0; i < n; i++)
            {
                array[0, i] = i.ToString();
                array[1, i] = Math.Pow(n, i).ToString();
                array[2, i] = (Math.Pow(n, i) + i).ToString();
            }
            Printing.array2Dim(array);

        }
    }

    public class Array2DSoal7
	{
		//n = 7
		//0	1	2	3	4	5	6
		//7	8	9	10	11	12	13
		//14	15	16	17	18	19	20

		public Array2DSoal7()
		{
			Console.WriteLine("\n== Soal 07 Array 2D");
			Console.Write("Masukkan nilai n: ");
			int n = int.Parse(Console.ReadLine());
			Console.WriteLine();

			string[,] array = new string[3, n];

			for (int i = 0; i < n; i++)
			{
				array[0, i] = i.ToString();
				array[1, i] = (n + i).ToString();
				array[2, i] = (n * 2 + i).ToString();
			}
			Printing.array2Dim(array);
		}
	}

	public class Array2DSoal8
	{
		//n = 7
		//0	1	2	3	4	5	6
		//0	2	4	6	8	10	12
		//0	3	6	9	12	15	18
		public Array2DSoal8()
		{
			Console.WriteLine("== Soal 08 Array 2D ===");
			Console.Write("Masukkan nilai n: ");
			int n = int.Parse(Console.ReadLine());
			Console.WriteLine();

			string[,] array = new string[3, n];

			for (int i = 0; i < n; i++)
			{
				array[0, i] = i.ToString();
				array[1, i] = (i * 2).ToString();
				array[2, i] = (i * 2 + i).ToString();
			}
			Printing.array2Dim(array);
		}
	}

	public class Array2DSoal9
	{
		//n1 = 7, n2 = 3
		//0	1	2	3	4	5	6
		//0	3	6	9	12	15	18
		//18	15	12	9	6	3	0
		public Array2DSoal9()
		{
			Console.WriteLine("== Soal 09 Array 2D ===");
			Console.Write("Masukkan nilai n1: ");
			int n1 = int.Parse(Console.ReadLine());
			Console.Write("Masukkan nilai n2: ");
			int n2 = int.Parse(Console.ReadLine());
			Console.WriteLine();

			string[,] array = new string[3, n1];

			for (int i = 0; i < n1; i++)
			{
				array[0, i] = i.ToString();
				array[1, i] = (i * n2).ToString();
				array[2, i] = ((n1 - 1 - i) * n2).ToString();
			}
			Printing.array2Dim(array);
		}
	}

    public class Array2DSoal10
    {
        //n1= 7, n2=3
        //0	1	2	3	4	5	6
        //0	3	6	9	12	15	18
        //0	4	8	12	16	20	24
        public Array2DSoal10()
        {
            Console.WriteLine("== Soal 10 Array 2D ===");
            Console.WriteLine();
            Console.Write("Masukkan nilai n1: ");
            int n1 = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai n2: ");
            int n2 = int.Parse(Console.ReadLine());
            Console.WriteLine();

            string[,] array = new string[3, n1];

            for (int i = 0; i < n1; i++)
            {
                array[0, i] = i.ToString();
                array[1, i] = (i * n2).ToString();
                array[2, i] = (i * n2 + i).ToString();
            }
            Printing.array2Dim(array);
        }
    }
}
