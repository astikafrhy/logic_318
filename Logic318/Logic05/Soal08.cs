﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
    public class Soal08
    {
        public Soal08()
        {
            // 3    9   27  81  243 729 2187
            int angka = 3;
            Console.Write("Masukkan banyak inputan (N) : ");
            int n = int.Parse(Console.ReadLine());

            string[] arrString = new string[n];


            for (int i = 0; i < n; i++)
            {
                Console.Write(angka + "\t");
                angka *= 3;
            }
            Printing.array1Dim(arrString);


            //cara2
            //for (int i = 0; i < n; i++)
            //{
            //    Console.Write((i * 3 + 3) + "\t");
            //}
        }
    }
}
