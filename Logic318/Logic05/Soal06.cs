﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
    internal class Soal06
    {
        public Soal06()
        {
            Console.Write("Masukan jumlah bilangan angka (N) : ");
            int n = int.Parse(Console.ReadLine());

            string[] arrString = new string[n];

            // int angka = 1;
            //for (int i = 1 ; i <= n; i++)
            //{
            //    string ubahAngka = angka.ToString();

            //    if (i % 3 == 0)
            //        Console.Write(" " + ubahAngka.Replace(ubahAngka, "*") + " ");
            //    else
            //        Console.Write(" " + ubahAngka + " ");
            //  angka += 4;
            //}
            //Console.WriteLine();

            //cara2 
            //for(int i = 1; i < n; i++)
            //{
            //    if( i % 3 == 0)
            //        Console.Write("*\t");
            //    else
            //        Console.Write($"({i * 4 - 3}\t");
            //}

            //cara3
            for (int i = 1; i <= n; i++)
            {
                Console.Write(i % 3 != 0 ? $"{i * 4 - 3}\t" : "*\t");
            }
            Printing.array1Dim(arrString);
        }
    }
}
