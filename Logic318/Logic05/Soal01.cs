﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility;

namespace Logic05
{
    public class Soal01
    {
        public Soal01()
        {
            Console.Write("Masukkan n  : ");
            int n = int.Parse(Console.ReadLine());
            string[] arrString = new string[n];
            //  1   3   5   7   9   11  13
            //int angka = 1;
            
            //for (int i = 0; i < n; i++)
            //{
            //    Console.Write(angka + "\t");
            //    angka += 2;

            //    arrString[i] = i.ToString();
            //}


            //cara 2
            //Console.Write("Masukkan banyak inputan (N) : ");
            //int n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                Console.Write((i * 2 + 1) + "\t");
            }
            Printing.array1Dim(arrString);

        }

    }
}
