﻿
//Konversi();
//operatorAritmatika();
//operatorModulus();
//operatorPenugasan();
//operatorPerbandingan();
//operatorLogika();
//ElseStatement();
//IfNested();
//ternary();
//manipulasiString();
//splitJoin();
substring();

Console.ReadKey();

static void Konversi()
{
    int myInt = 10;
    double myDouble = 5.25;
    bool myBool = true;

    Console.WriteLine("convert int to string : " + Convert.ToString(myInt)); //cara1 
    Console.WriteLine("convert int to string : " + myInt.ToString()); //cara2
    Console.WriteLine("convert int to double : " + Convert.ToDouble(myInt)); 

    Console.WriteLine("convert double to string : " + Convert.ToString(myDouble));
    Console.WriteLine("convert double to int : " + Convert.ToInt32(myDouble));

    Console.WriteLine("convert bool to string : " + Convert.ToString(myBool)); //cara1
    Console.WriteLine("convert bool to string : " + myBool.ToString()); //cara2

}

static void operatorAritmatika()
{
    int hasil=0;
    Console.WriteLine("=== OPERATOR ARITMATIKA ===");
    Console.Write("Masukkan jumlah Mangga : ");
    int mangga = int.Parse(Console.ReadLine());

    Console.Write("Masukkan jumlah Apel : ");
    int apel = int.Parse(Console.ReadLine());

    hasil = mangga + apel;

    Console.WriteLine($"Jumlah mangga + apel = {hasil}");
   
}

static void operatorModulus()
{
    int hasil = 0;
    Console.WriteLine("=== OPERATOR MODULUS ===");
    Console.Write("Masukkan jumlah Semangka : ");
    int semangka = int.Parse(Console.ReadLine());
    Console.Write("Masukkan jumlah Jeruk : ");
    int jeruk = int.Parse(Console.ReadLine());

    hasil = semangka % jeruk;

    Console.WriteLine("Jumlah Semangka % Jeruk = " + hasil);
}

static void operatorPenugasan()
{
    int durian = 9;
    int nangka = 3;
    Console.WriteLine("=== OPERATOR PENUGASAN ===");

    durian -= 6;
    nangka += 2;

    Console.WriteLine("Jumlah Durian = " + durian);
    Console.WriteLine("Jumlah Nangka = " + nangka);
}

static void operatorPerbandingan()
{
    //int mangga, apel = 0;
    Console.WriteLine("=== OPERATOR PERBANDINGAN ===");
    Console.Write("Masukkan jumlah mangga : ");
    int mangga = int.Parse(Console.ReadLine());

    Console.Write("Masukkan jumlah apel : ");
    int apel = int.Parse(Console.ReadLine());

    Console.WriteLine("Hasil Perbandingan : ");
    Console.WriteLine($"Mangga + Apel > : {mangga > apel}");
    Console.WriteLine($"Mangga + Apel < : {mangga < apel}");
    Console.WriteLine($"Mangga + Apel >= : {mangga >= apel}");
    Console.WriteLine($"Mangga + Apel <= : {mangga <= apel}");
    Console.WriteLine($"Mangga + Apel != : {mangga != apel}");
    Console.WriteLine($"Mangga + Apel == : {mangga == apel}");

}

static void operatorLogika()
{
    Console.WriteLine("=====OPERATOR LOGIKA====");
    Console.Write("masukkan umur = ");
    int umur = int.Parse(Console.ReadLine());
    Console.Write("masukkan password = :");
    string password = Console.ReadLine();

    bool isAdult = umur > 18; //kondisi pertama
    bool isPasswordValid = password == "admin"; //kondisi kedua

    if (isAdult && isPasswordValid)
    {
        Console.WriteLine("Sudah Dewasa dan Password Valid");
    }
    else if (isAdult && !isPasswordValid)
    {
        Console.WriteLine("Sudah dewasa tetapi password invalid");
    }
    else if (!isAdult && isPasswordValid)
    {
        Console.WriteLine("Belum dewasa tetapi password Valid");
    }
    else
    {
        Console.WriteLine("Belum dewasa dan password invalid");
    }
}
static void ElseStatement()
{
    int ratarata = 0;
    Console.WriteLine("=== ELSE STATEMENT ===");
    Console.Write("Masukkan nilai MTK : ");
    int mtk = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai Fisika : ");
    int fisika = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai Kimia : ");
    int kimia = int.Parse(Console.ReadLine());

    ratarata = (mtk + fisika + kimia) / 3;
    Console.WriteLine("Nilai Rata-rata : " + ratarata);
    Console.WriteLine("");

    if( ratarata >= 0 && ratarata <= 60)
    {
        Console.WriteLine("Maaf");
        Console.WriteLine("Kamu Gagal");
    }
    else if (ratarata >= 61 && ratarata <= 100)
    {
        Console.WriteLine("Selamat");
        Console.WriteLine("Kamu Berhasil");
        Console.WriteLine("Kamu Hebat");
    }
    else
    {
        Console.WriteLine("Nilai yang anda masukkan salah");
    }
}

static void IfNested()
{
    int kode_baju = 0, harga = 0;
    string merk = "", ukuran = "";

    Console.WriteLine("=== IMP Fashion ===");
    Console.Write("Masukkan Kode Baju = ");
    kode_baju = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Kode Ukuran = ");
    ukuran = Console.ReadLine().ToUpper();

    if (kode_baju == 1)
    {
        merk = "IMP";

        if(ukuran == "S")
        {
            harga = 200000;
        }

        else if (ukuran == "M")
        {
            harga = 220000;
        }
        else
        {
            harga = 250000;
        }
    }

    else if (kode_baju == 2)
    {
        merk = "Prada";

        if (ukuran == "S")
        {
            harga = 150000;
        }

        else if (ukuran == "M")
        {
            harga = 160000;
        }
        else
        {
            harga = 170000;
        }
    }

    else if (kode_baju == 3)
    {
        merk = "Gucci";

        if (ukuran == "S")
        {
            harga = 200000;
        }

        else if (ukuran == "M")
        {
            harga = 200000;
        }
        else
        {
            harga = 200000;
        }
    }
    else
    {
        Console.WriteLine("");
        Console.WriteLine("Pilihan Tidak Tersedia!");
    }
    Console.WriteLine("");
    Console.WriteLine("Merk Baju = " + merk);
    Console.WriteLine("Harga = " + harga);
}

static void ternary()
{
    int x, y;
    Console.WriteLine("=== TERNARY ===");
    Console.Write("Masukkan nilai x : ");
    x = int.Parse(Console.ReadLine());
    Console.Write("Masukkan nilai y : ");
    y = int.Parse(Console.ReadLine());

    string hasilTernary = x < y ? "nilai x lebih kecil dari nilai y" : "nilai x lebih besar dari nilai y";
    
    Console.WriteLine(hasilTernary);
}

static void manipulasiString()
{
    Console.WriteLine("=== MANUPULASI STRING ===");
    Console.Write("Masukkan kata : ");
    string kata = Console.ReadLine();

    string remove = kata;
    string insert = kata;
    string replace = kata;

    Console.WriteLine($"Panjang karakter dari {kata} = {kata.Length}");
    Console.WriteLine($"ToUpper dari {kata} = {kata.ToUpper()}");
    Console.WriteLine($"ToLower dari {kata} = {kata.ToLower()}");
    Console.WriteLine($"Hapus dari {remove} = {remove.Remove(11)}");
    Console.WriteLine($"Insert dari {insert} = {insert.Insert(5, "Banget")}");
    Console.WriteLine($"Ubah dari {replace} = {replace.Replace("id", "net")}");


}

static void splitJoin()
{
    Console.WriteLine("=== SPLIT JOIN ===");
    Console.Write(" Masukkan kalimat : ");
    string kalimat = Console.ReadLine();

    string[] kataArray = kalimat.Split(" ");

    foreach (string kata in kataArray)
    {
        Console.WriteLine(kata);
    }
    Console.WriteLine(string.Join("+", kataArray));
}

static void substring()
{
    Console.WriteLine("=== SUBSTRING ===");
    Console.Write("masukkan kalimat : ");
    string kalimat = Console.ReadLine();

    Console.WriteLine($"String Tahun = {kalimat.Substring(1, 4)}");
    Console.WriteLine($"String Bulan ={ kalimat.Substring(5, 2)}");
    Console.WriteLine($"Kode Lokasi = {kalimat.Substring(7, 9)} ");
    Console.WriteLine($"Nomor = {kalimat.Substring(9)}");
}


