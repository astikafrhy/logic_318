﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    internal class SherlockAndArray
    {
        public SherlockAndArray()
        {
            Console.WriteLine();
            Console.Write("masukkan deret  : ");
            string[] deret = Console.ReadLine().Split(' ');
            int[] arr = Array.ConvertAll(deret, int.Parse);

            int LeftSum = 0;
            int RightSum = arr.Sum();
            string hasil = "NO";

            for (int i = 0; i < arr.Length; i++)
            {
                RightSum -= arr[i];
                if (LeftSum == RightSum)
                {
                    hasil = "YES";
                    break;
                }
                LeftSum += arr[i];
            }
            
            Console.WriteLine(hasil);
            Console.WriteLine($"Jumlah Kiri : {LeftSum} , Jumlah Kanan : {RightSum}");
        }
    }
}
