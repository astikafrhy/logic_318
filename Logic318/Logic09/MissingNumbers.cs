﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    internal class MissingNumbers
    {
        //public MissingNumbers()
        //{
        //    Console.Write("Masukkan data1: ");
        //    int[] data1 = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
        //    Console.Write("Masukkan data2: ");
        //    int[] data2 = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);

        //    List<int> list1 = data1.ToList();
        //    List<int> list2 = data2.ToList();
        //    List<int> list3 = new List<int>();


        //    for (int i = 0; i < list1.Count; i++)
        //    {
        //        if (!list2.Contains(list1[i]))
        //            list3.Add(list1[i]);
        //        if (list2.Contains(list1[i]))
        //            list2.Remove(list1[i]);
        //    }

        //    for (int i = 0; i < list2.Count; i++)
        //    {
        //        if (!list1.Contains(list2[i]))
        //            list3.Add(list2[i]);
        //        if (list1.Contains(list2[i]))
        //            list1.Remove(list2[i]);
        //    }

        //    Console.WriteLine();

        //    for (int i = 0; i < list3.Count; i++)
        //    {
        //        Console.WriteLine(list3[i]);
        //    }
        //}

        //cara2
        public MissingNumbers()
        {
            Console.Write("angka 1 : ");
            List<int>angka1 = Console.ReadLine().Split(' ').Select(int.Parse).ToList();   

            Console.Write("angka 2 : ");
            List<int>angka2 = Console.ReadLine().Split(' ').Select(int.Parse).ToList();

            for (int  i = 0; i < angka1.Count; i++)
            {
                for(int j = 0; j < angka2.Count; j++)
                {
                    if(angka1[i] == angka2[j])
                    {
                        angka1.RemoveAt(i);
                        angka2.RemoveAt(j);
                        i--;j--;
                        break;
                    }
                }
            }
            angka2.Sort();
            Console.WriteLine($"Sisa {String.Join(" ",angka2.Distinct())}");
        }
    }
}
