﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    internal class DatetimeReview  
    {
        public DatetimeReview()
        {
            Console.WriteLine();
            Console.WriteLine("=== DATETIME REVIEW ===");
            Console.Write("Tanggal Awal  : ");
            string[] dtAwal   = Console.ReadLine().Split('T');
            string[] tglAwal  = dtAwal[0].Split('-');
            string[] wktAwal  = dtAwal[1].Split(':');
            DateTime dateAwal = new DateTime(int.Parse(tglAwal[0]), int.Parse(tglAwal[1]), int.Parse(tglAwal[2]),
                                int.Parse(wktAwal[0]), int.Parse(wktAwal[1]), int.Parse(wktAwal[2]));

            Console.Write("Tanggal Akhir : ");
            string[] dtAkhir   = Console.ReadLine().Split('T');
            string[] tglAkhir  = dtAkhir[0].Split('-');
            string[] wktAkhir  = dtAkhir[1].Split(':');
            DateTime dateAkhir = new DateTime(int.Parse(tglAkhir[0]), int.Parse(tglAkhir[1]), int.Parse(tglAkhir[2]),
                                 int.Parse(wktAkhir[0]), int.Parse(wktAkhir[1]), int.Parse(wktAkhir[2]));

            TimeSpan dt = dateAkhir - dateAwal;

            Console.WriteLine();
            Console.WriteLine($"{dateAwal.ToString("yyyy-MM-ddThh:mm:ss")} - {dateAkhir.ToString("yyyy-MM-ddThh:mm:ss")}");

            Console.WriteLine($"Selisih hari : {dt.Days}");
            Console.WriteLine($"Selisih jam : {dt.Hours}");
            Console.WriteLine($"Selisih menit : {dt.Minutes}");
            Console.WriteLine($"Selisih detik : {dt.Seconds}");
           // Console.WriteLine($"tahun : {dt.Year}");

            Console.WriteLine($"Selisih Total Jam : {dt.TotalHours}");
        }
    }
}
