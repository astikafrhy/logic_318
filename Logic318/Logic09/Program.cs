﻿namespace Logic09
{
    public class Program
    {
        public Program()
        {
            Menu();
        }
        static void Main(string[] args)
        {
            Menu();
        }
        private static void Menu()
        {
            Console.WriteLine();
            Console.WriteLine("==== Welcome to Day 09 ====");
            Console.WriteLine("| 1. Missing Numbers      |");
            Console.WriteLine("| 2. Sherlock and Array   |");
            Console.WriteLine("| 3. Datetime Review      |");
            Console.WriteLine("| 4. Pairs                |");
            Console.WriteLine("| 5. Ice Cream Parlor     |");
            Console.WriteLine("| 6. Recursive Digit Sum  |");
            //Console.WriteLine("| 7. Find The Median     |");
            ////Console.WriteLine("| 8. Gamestone           |");
            ////Console.WriteLine("| 9. Making Anagram      |");
            ////Console.WriteLine("| 10. Two String         |");

            Console.WriteLine();
            Console.Write("Masukkan no soal : ");
            int soal = int.Parse(Console.ReadLine());

            string answer = "t";
            while (answer.ToLower() == "t")
            {
                switch (soal)
                {
                    case 1:
                        MissingNumbers soal1 = new MissingNumbers();
                        kembali();
                        break;
                    case 2:
                        SherlockAndArray soal2 = new SherlockAndArray();
                        kembali();
                        break;
                    case 3:
                        DatetimeReview soal3 = new DatetimeReview();
                        kembali();
                        break;
                    case 4:
                        Pairs soal4 = new Pairs();
                        kembali();
                        break;
                    case 5:
                        IceCreamParlor soal5 = new IceCreamParlor();
                        kembali();
                        break;
                    case 6:
                        RecursiveDigitSum soal6 = new RecursiveDigitSum();
                        kembali();
                        break;
                    default:
                        break;
                }
                Console.WriteLine();
                Console.Write("Press any key...");
                Console.WriteLine();
                Console.ReadKey();
            }
            static void kembali()
            {
                Console.Write("\nApakah ingin kembali ke menu ? (y/n) ");
                string menuInput = Console.ReadLine().ToLower();
                if (menuInput == "y")
                {
                    Console.Clear();
                    Menu();
                }
                else
                {
                    Console.WriteLine("Terima kasih!");
                    Console.WriteLine("Silahkan tekan Enter kembali...");
                }
            }
        }
    }
}
