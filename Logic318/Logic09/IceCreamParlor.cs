﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic09
{
    internal class IceCreamParlor
    {
        public IceCreamParlor()
        {
			Console.WriteLine("=== Ice Cream Parlor ===");
			Console.Write("Masukkan jumlah uang: ");
			int uang = int.Parse(Console.ReadLine());
			Console.Write("Masukkan data ice cream: ");
			int[] eskrim = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);
			int min = 0, jenis1 = 0, jenis2 = 0;
			for (int i = 0; i < eskrim.Length; i++)
			{
				for (int j = 0; j < eskrim.Length; j++)
				{
					if (i == j)
						continue;
					int sum = eskrim[i] + eskrim[j];
					if (sum <= uang && sum >= min)
					{
						min = sum;
						jenis1 = i + 1;
						jenis2 = j + 1;
					}
				}
			}
			Console.WriteLine($"{jenis1} dan {jenis2}");

		}
	}
}
