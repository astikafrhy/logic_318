﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class Sorting
    {
        public static int[] Insertion(int[] arr)
        {
            //5   4   3   2   1
            //4   5   3   2   1
            //3   4   5   2   1
            //2   3   4   5   1
            //......
            //1   2   3   4   5

            //int[] result = new int[arr.Length];
            for (int i = 1; i < arr.Length; i++)
            {
                for (int j = i; j > 0; j--)
                {
                    if (arr[j] < arr[j-1])
                    {
                        var temp = arr[j - 1];
                        arr[j - 1] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
            return arr;
        }
    }
}

    
           
