﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public static class Printing
    {
        public static void array1Dim(string[] arr)
        { 
            for(int i = 0; i < arr.Length; i++)
            {
                Console.Write($"{arr[i]}\t");
            }
        }

        public static void array2Dim(string[,] arr)
        {
            for (int row = 0; row < arr.GetLength(0); row++)
            {
                for(int col = 0; col < arr.GetLength(1); col++)
                {
                     
                Console.Write($"{arr[row, col]}\t");
                }
                Console.WriteLine();
            }
        }

        public static void array2Dim(int[,] arr)
        {
            for (int row = 0; row < arr.GetLength(0); row++)
            {
                for (int col = 0; col < arr.GetLength(1); col++)
                {

                    Console.Write($"{arr[row, col]}\t");
                }
                Console.WriteLine(\n);
            }
        }

    }
}
