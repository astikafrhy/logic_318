﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class Camelcase
    {
        public Camelcase()
        {
            Console.WriteLine();
            Console.Write("Masukkan Kalimat : ");
            string kalimat = Console.ReadLine();
            int jumlah = 0;

            for(int i = 0; i < kalimat.Length; i++)
            {
                if(i == 0 || char.IsUpper(kalimat[i]))
                {
                    Console.WriteLine();
                    jumlah++;
                }
                    Console.Write(kalimat[i]);       
            }
            Console.WriteLine();
            Console.WriteLine($"Jumlah kata = {jumlah}");

        }

        //public Camelcase()
        //{
        //    // astika
        //    // ABCDEFGHIJKLMNOPQRSTUVWXYZ
        //    Console.Write("Masukkan kata : ");
        //    string kata = Console.ReadLine();

        //    string libString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        //    for(int i = 0; i < kata.Length; i++)
        //    {
        //        Console.WriteLine($"Index string : {kata[i]}, {libString.IndexOf(kata[i])}");
        //    }
        //}
    }
}
