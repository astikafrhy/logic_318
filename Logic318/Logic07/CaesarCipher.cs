﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class CaesarCipher
    {
        public CaesarCipher()
        {
            Console.WriteLine();
            Console.Write("Masukkan kalimat : ");
            char[] kalimat= Console.ReadLine().ToCharArray();
            Console.Write("Masukkan rotasi  : ");
            int rotasi = int.Parse(Console.ReadLine());

			rotasi = rotasi % 26;

            string lower_case = "abcdefghijklmnopqrstuvwxyz";
            string upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

			string rotAlfabet = lower_case.Substring(rotasi) + lower_case.Substring(0, rotasi);
			string rotAlfabetUpper = upper_case.Substring(rotasi) + upper_case.Substring(0, rotasi);

			for (int i = 0; i < kalimat.Length; i++)
			{
				if (upper_case.Contains(kalimat[i]))
				{
					int index = upper_case.IndexOf(kalimat[i]);
					kalimat[i] = rotAlfabetUpper[index];
				}
				if (lower_case.Contains(kalimat[i]))
				{
					int index = lower_case.IndexOf(kalimat[i]);
					kalimat[i] = rotAlfabet[index];
				}
			}

			foreach (char c in kalimat)
			{
				Console.Write(c);
			}

			Console.WriteLine();
		}


	}
    
}
