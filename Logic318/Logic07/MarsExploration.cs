﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class MarsExploration
    {
        public MarsExploration()
        {
            Console.WriteLine();
            Console.Write("masukkan kode : ");
            char[] kode = Console.ReadLine().ToUpper().ToCharArray();

            int hitung = 0;
            int hitung2 = 0;
            if (kode.Length % 3 != 0)
            {
                Console.WriteLine("Masukkan kode yang benar!");
            }
            else
            {
                for (int i = 0; i < kode.Length; i += 3)
                {
                    if (kode[i] != 'S' || kode[i + 1] != 'O' || kode[i + 2] != 'S')
                        hitung++;
                    else
                        hitung2++;
                }
                Console.WriteLine($"Total kode yang salah : {hitung}");
                Console.WriteLine($"Total kode yang benar : {hitung2}");  
            }
        }
    }
}