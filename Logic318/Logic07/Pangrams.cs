﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class Pangrams
    {
        public Pangrams()
        {
            Console.WriteLine();
            Console.Write("Masukkan kalimat : ");
            string kal = Console.ReadLine().ToUpper();

            string upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            bool pangram = true;

            for (int i = 0; i < upper_case.Length; i++)
            {
                if(!kal.Contains(upper_case[i]))
                {
                    pangram = false;
                }
            }
            if (pangram)
                Console.WriteLine("Pangram");
            else
                Console.WriteLine("Not Pangram");
            //ternary
           //Console.WriteLine(pangram ? "pangram" : "not pangram" );   
        }
    }
}
