﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class StrongPassword
    {
        public StrongPassword()
        {
            Console.Write("Masukkan Password : ");
            string pass = Console.ReadLine();

            string numbers = "0123456789";
            string lower_case = "abcdefghijklmnopqrstuvwxyz";
            string upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string special_characters = "!@#$%^&*()-+";

            int hitung = 0;

            if(pass.IndexOfAny(numbers.ToCharArray()) == -1)
            {
                hitung += 1;
            }
            if (pass.IndexOfAny(lower_case.ToCharArray()) == -1)
            {
                hitung += 1;
            }
            if (pass.IndexOfAny(upper_case.ToCharArray()) == -1)
            {
                hitung += 1;
            }
            if (pass.IndexOfAny(special_characters.ToCharArray()) == -1)
            {
                hitung += 1;
            }

            if(pass.Length < 6)
            {
                hitung = hitung > 6 - pass.Length ? hitung : 6 - pass.Length;
            }
            Console.WriteLine(hitung);

        }
    }
}
