﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class MakingAnagram
    {
        public MakingAnagram()
        {
            Console.WriteLine();
            Console.Write("Masukkan kata 1 : ");
            string kata1 = Console.ReadLine();

            Console.Write("Masukkan kata 2 : ");
            string kata2 = Console.ReadLine();

            int panjangK1 = kata1.Length;
            int panjangK2 = kata2.Length;

            List<char> ListK1 = new List<char>(panjangK1);
            List<char> ListK2 = new List<char>(panjangK2);

            ListK1 = kata1.ToList();
            ListK2 = kata2.ToList();

            int total = panjangK1 + panjangK2;
            int count = 0;

            for (int i = 0; i < panjangK1; i++)
            {
                if (ListK2.Contains(ListK1[i]))
                {
                    count++;
                    ListK2.Remove(ListK1[i]);
                }
            }
            Console.WriteLine($"Jumlah huruf yang berbeda adalah {total - (2 * count)}");
        }
    }
}
