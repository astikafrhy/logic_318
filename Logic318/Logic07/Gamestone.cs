﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class Gamestone
    {
        public Gamestone()
        {
            Console.Write("Masukkan jumlah data : ");
            int jmlh = int.Parse(Console.ReadLine());

            string gemstone = "";
            int[] alfabet = new int[26];
            for (int i = 1; i <= jmlh; i++)
            {
                Console.Write($"Data ke-{i} : ");
                gemstone = Console.ReadLine();

                //distinct
                char[] uniqueCharArray = gemstone.ToCharArray().Distinct().ToArray();
                string resultString = new string(uniqueCharArray);

                for (int j = 0; j < resultString.Length; j++)
                {
                    //pengurangan di char
                    alfabet[resultString[j] - 'a']++;
                }
            }
            int count = 0;
            //pencetakan string berulang/index
            for (int k = 0; k < alfabet.Length; k++)
            {
                //ngecek alfabet
                if (alfabet[k] == jmlh)
                {
                    //casting pemaksaan tipe variabel
                    Console.WriteLine((char)((k) + 'a'));
                    count++;
                }
            }
            Console.WriteLine(count);
        }
    }
}
