﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class TwoString
    {
        public TwoString()
        {
            Console.WriteLine();
            Console.Write("Masukkan kata 1 : ");
            string kata1 = Console.ReadLine();

            Console.Write("Masukkan kata 2 : ");
            string kata2 = Console.ReadLine();
             
            bool isTwoString = false; 

            for (int i = 0; i < kata1.Length; i++)
            {
                for(int j = 0; j < kata2.Length; j++)
                {
                    if(kata1[i] == kata2[j])
                    {
                        isTwoString = true;
                        break;
                    }
                }
            }
            Console.WriteLine(isTwoString ? "YES" : "NO");
        }
    }
}
