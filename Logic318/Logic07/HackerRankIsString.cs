﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic07
{
    internal class HackerRankIsString
    {
        public HackerRankIsString()
        {
            Console.WriteLine();
            Console.Write("Masukkan kalimat : ");
            string kal = Console.ReadLine();
            
            Console.Write("Masukkan kata yang akan dicari : ");
            string cari = Console.ReadLine();

            int index = 0;

            for(int i = 0; i < kal.Length; i++)
            {
                if(index < cari.Length && kal[i] == cari[index])
                {
                    index++;
                }
            }
            Console.WriteLine(index == cari.Length ? "YES" : "NO");
        }
    }
}
