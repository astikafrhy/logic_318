﻿//soal1();
//soal2();
//soal3();
//soal4();
//soal5();
//soal6(); 
soal7();
//soal8();
Console.ReadKey();

static void soal1()
{
    double upah = 0, totalUpah = 0, lembur = 0, totalGaji = 0;   
    Console.WriteLine("=== GAJI KARYAWAN ===");
    Console.Write("Golongan  : ");
    int golongan = int.Parse(Console.ReadLine());
    Console.Write("Jam Kerja : ");
    int jamKerja = int.Parse(Console.ReadLine());

    switch (golongan)
    {
        case 1:
            upah = 2000;
                break;
        case 2:
            upah = 3000;
            break;
        case 3:
            upah = 4000;
            break;
        case 4:
            upah = 5000;
            break;
        default:
            Console.WriteLine("=======================");
            Console.WriteLine("Golongan Tidak Tersedia");
            break;
    }
    if(jamKerja <= 40)
    {
        totalUpah = upah * jamKerja;
        lembur = 0;
        totalGaji = totalUpah + lembur;
    }
    else if(jamKerja >= 40)
    {
        totalUpah = upah * 40;
        lembur = 1.5 * upah * (jamKerja - 40);
        totalGaji = totalUpah + lembur;
    }
    Console.WriteLine("=======================");
    Console.WriteLine($"Upah      : {totalUpah, 9}");
    Console.WriteLine($"lembur    : {lembur, 9}");
    Console.WriteLine($"Total     : {totalGaji, 9}");
}

static void soal2()
{
    Console.WriteLine("=== KALIMAT JADI KATA===");
    Console.Write("Masukkan Kalimat : ");

    string[] kataArray = Console.ReadLine().Split();

    for (int i = 0; i < kataArray.Length; i++)
    {
        Console.WriteLine($"Kata {i + 1} = {kataArray[i]}");
    }
    Console.WriteLine("");
    Console.WriteLine($"Total kata adalah {kataArray.Length}");

}

static void soal3()
{
    Console.WriteLine("=== HILANG KATA (REPLACE) ===");
    Console.Write("Masukkan Kalimat : ");
    string[] kalimat = Console.ReadLine().Split();

    for (int i = 0; i < kalimat.Length; i++)
    {
        for (int j = 0; j < kalimat[i].Length; j++)
        {
            if (j == 0)
            {
                Console.Write(kalimat[i][j]);
            }
            else if (j == kalimat[i].Length - 1)
            {
                Console.Write(kalimat[i][j]);
            }
            else
            {
                Console.Write(kalimat[i].Replace(kalimat[i], "*"));
            }

        }
        Console.Write(" ");
    }
}

static void soal4()
{
    Console.WriteLine("=== HILANG KATA 2 (REPLACE) ===");
    Console.Write("Masukkan Kalimat : ");
    string[] kalimat = Console.ReadLine().Split();

    for (int i = 0; i < kalimat.Length; i++)
    {
        for (int j = 0; j < kalimat[i].Length; j++)
        {
            if (j == 0)
            {
                Console.Write(kalimat[i].Replace(kalimat[i], "*"));
            }
            else if (j == kalimat[i].Length - 1)
            {
                Console.Write(kalimat[i].Replace(kalimat[i], "*"));
            }
            else
            {
                Console.Write(kalimat[i][j]);
            }
        }
        Console.Write(" ");
    }
}

static void soal5()
{
    Console.WriteLine("=== HILANG KATA 3 (REPLACE) ===");
    Console.Write("Masukkan Kalimat : ");
    string[] kalimat = Console.ReadLine().Split();

    for (int i = 0; i < kalimat.Length; i++)
    {
        for (int j = 0; j < kalimat[i].Length; j++)
        {
            if (j == 0)
                Console.Write(kalimat[i].Remove(0));
            else 
                Console.Write(kalimat[i][j]);

        }
        Console.Write(" ");
    }
}

static void soal6()
{
    Console.WriteLine("=== REPLACE ANGKA ===");
    Console.Write("Masukan jumlah bilangan angka: ");
    int n = int.Parse(Console.ReadLine());

    int angka = 1;
    for (int i = 1; i <= n; i++)
    {
        angka *= 3;
        string ubahAngka = angka.ToString();

        if (i % 2 == 0)
            Console.Write(" " + ubahAngka.Replace(ubahAngka, "*") + " ");
        else
            Console.Write(" " + ubahAngka + " ");
    }
    Console.WriteLine();

}

static void soal7()
{
    int number;
    Console.Write("Masukkan banyak angka yang ingin dimunculkan : ");
    number = int.Parse(Console.ReadLine());

    int[] numberArray = new int[number];

    for (int i = 0; i < number; ++i)
    {
        if (i <= 1)
            numberArray[i] = 1;
        else
            numberArray[i] = numberArray[i - 2] + numberArray[i - 1];
    }
    Console.Write(string.Join(",", numberArray));

}

static void soal8()
{
    Console.WriteLine("=== PERSEGI PANJANG ===");
    Console.Write("Masukkan panjang persegi: ");
    int n = int.Parse(Console.ReadLine());

    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= n; j++)
        {
            if (i == 1)
                Console.Write(j);
            else if (i == n)
                Console.Write(n - j + 1);
            else if (j == 1 || j == n)
                Console.Write("*");
            else
                Console.Write(" ");
        }
        Console.WriteLine(" ");
    }
}