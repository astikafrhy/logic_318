﻿//soal1();
//soal2();
//soal3();
soal4();
//soal5();
//soal6();
Console.ReadKey();

static void soal1()
{
    int r = 0;
    double luas = 0, keliling = 0;
    Console.WriteLine("=== LUAS DAN KELILING LINGKARAN ===");
    while (true)
    {
        Console.Write("Masukkan jari-jari (r) : ");
        r = int.Parse(Console.ReadLine());

        if (r < 0)
            Console.WriteLine("inputan salah");
        else
            break;
    } 

    luas = (22 * r * r)/7;
    keliling = (2 * r * 22)/7;

    Console.WriteLine("===================================");
    Console.WriteLine("Hasil Keliling Lingkaran = " + keliling + " cm");
    Console.WriteLine("Hasil Luas Lingkaran     = " + luas + " cm\xB2");  

}

static void soal2()
{
    int s = 0;
    double luas = 0, keliling = 0;
    Console.WriteLine("=== LUAS DAN KELILING PERSEGI ===");
    while (true)
    {
        Console.Write("Masukkan sisi (s) : ");
        s = int.Parse(Console.ReadLine());

        if (s < 0)
            Console.WriteLine("inputan salah");
        else
            break;
    }
    luas = s * s;
    keliling = 4 * s;

    Console.WriteLine("=================================");
    Console.WriteLine("Hasil Keliling Lingkaran = " + keliling + " cm");
    Console.WriteLine("Hasil Luas Lingkaran     = " + luas + " cm\xB2");
}


static void soal3()
{
    double hasil = 0, angka = 0, pembagi = 0;
    Console.WriteLine("=== HASIL MODULUS ===");
    while (true)
    {
        Console.Write("Masukkan angka : ");
        angka = int.Parse(Console.ReadLine());
        Console.Write("Masukkan pembagi : ");
        pembagi = int.Parse(Console.ReadLine());

        if (angka < 0 && pembagi < 0)
            Console.WriteLine("inputan salah");
        else
            break;
    }
    
    hasil = angka % pembagi;

    if (hasil == 0)
    {
        Console.WriteLine($"angka ({angka} % {pembagi}) adalah 0");
    }
    else if (hasil != 0)
    {
        Console.WriteLine($"angka ({angka} % {pembagi}) bukan 0 melainkan {hasil}");
    }
    else
    {
        Console.WriteLine("Nilai tidak diketahui");
    }

}

static void soal4()
{
    int batang, rokok, keuntungan, sisa;
    Console.WriteLine("=== ROKOK ===");
    while (true)
    {
        Console.Write("puntung yang dikumpulkan : ");
        rokok = int.Parse(Console.ReadLine());

        if (rokok < 0)
            Console.WriteLine("inputan salah");
        else
            break;
    }
        batang = rokok / 8;
        sisa = rokok % 8;

        Console.WriteLine($"Banyaknya batang yang dikumpulkan = {batang}");
        Console.WriteLine($"Sisa batang                       = {sisa}");

        keuntungan = batang * 500;
        Console.WriteLine($"Penghasilan yang didapatkan       = {keuntungan}");
    
}

static void soal5()
{
    int nilai = 0;
    Console.WriteLine("=== GRADE NILAI===");
    while (true)
    {
        Console.Write("Masukkan Nilai : ");
        nilai = int.Parse(Console.ReadLine());

        if (nilai < 0)
            Console.WriteLine("inputan salah");
        else
            break;
    }
    
    if(nilai >= 0 && nilai < 60)
    {
        Console.WriteLine("Grade C");
    }
    else if(nilai >= 60 && nilai < 80)
    {
        Console.WriteLine("Grade B");
    }
    else if ( nilai >= 80 && nilai <= 100)
    {
        Console.WriteLine("Grade A");
    }
    else
    {
        Console.WriteLine("Nilai yang anda masukan salah!");
    }

}

static void soal6()
{
    Console.WriteLine("=== GANJIL GENAP===");
    int angka = 0;
    while (true)
    {
        Console.Write("Masukkan angka : ");
        angka = int.Parse(Console.ReadLine());

        if (angka < 0)
            Console.WriteLine("inputan salah");
        else
            break;
    }

    // cara ternary
    //int hasil = angka % 2 == 0 ? $"angka {angka} adalah bilangan genap" : $"angka {angka} adalah bilangan ganjil";
    //Console.WriteLine(hasil);



    if (angka % 2 == 0)
    {
        Console.WriteLine("===============================");
        Console.WriteLine($"angka {angka} adalah bilangan genap");
    }
    else
    {
        Console.WriteLine("================================");
        Console.WriteLine($"angka {angka} adalah bilangan ganjil");
    }

}


